#include "GameFunctions.h"
#include "VectorFunctions.h"
using namespace std;

void AddToCardsOnTable(Card* c, vector<Card*> deckSuits[4]){
    deckSuits[(int)(c->getSuit())].push_back(c);
    sort(deckSuits[(int)(c->getSuit())].begin(), deckSuits[(int)(c->getSuit())].end(), compareCards());
}

vector<Card*> LegalMovesForASuit(vector<Card*> deck, vector<Card*> cardVector, int mySuit)      //mySuit is necessary for cases when the size of the vector is zero.
{
    vector<Card*> returnableVector;
    int vecSize = cardVector.size()-1;
    if(cardVector.size()==0)
    {
        Card* c = new Card((Suit)mySuit, SEVEN);
        returnableVector.push_back(SearchInDeck(deck, c));
        delete c;
        return returnableVector;
    }
    if(cardVector[0]->getRank()>=1)
    {
        Card* c = new Card((Suit)mySuit, (Rank)((int)cardVector[0]->getRank() - 1) );
        returnableVector.push_back(SearchInDeck(deck, c));
        delete c;
    }
    if(cardVector[vecSize]->getRank()<=11)
    {
        Card* c = new Card((Suit)mySuit, (Rank)((int)cardVector[cardVector.size()-1]->getRank() + 1) );
        returnableVector.push_back(SearchInDeck(deck, c));
        delete c;
    }

    return returnableVector;
}

vector<Card* > GeneratingLegalMoves(vector<Card*> deck, vector<Card*> deckSuits[4])
{
    vector<Card*> legalMoves;
    //player[i] is the player with the 7 of Spades. First thing that this player needs to do is throw the seven of spades. This will be the case with every player.
    //First thing that their card function will check is if they have the seven of spades.
    if(deckSuits[0].size()==0 && deckSuits[1].size()==0 && deckSuits[2].size()==0 && deckSuits[3].size()==0)   //No cards on the table. Only legal move is 7 of Spades
    {
        Card* c = new Card(SPADE, SEVEN);
        legalMoves.push_back(SearchInDeck(deck, c));
        delete c;
        return legalMoves;
    }
    for(int i=0; i<4; i++)
    {
        vector<Card*> returnedVector = LegalMovesForASuit(deck, deckSuits[i], i);
        for(int i=0; i<(int)returnedVector.size(); i++)
            legalMoves.push_back(returnedVector[i]);
    }
    return legalMoves;
}

void shuffle(vector<Card* > &cards_){
    int n = 52;
    while ( n > 1 ) {
        int k = (int) (lrand48() % n);
        --n;
        Card *c = cards_[n];
        cards_[n] = cards_[k];
        cards_[k] = c;
    }
}

void InvitePlayers(Player* players[4])
{
    for(int i=0; i<4; i++)
    {
        char input;
        cout<<"Is player "<<i+1<<" a human(h) or a computer(c)?"<<endl;
        cout<<">";
        cin>>input;
        assert (input == 'c' || input == 'C' || input =='h' || input == 'H');
        if(input=='h' || input=='H')
            players[i] = new HumanPlayer(0, HUMAN);
        else if (input=='c' || input=='C')
            players[i] = new ComputerPlayer(0, COMPUTER);
    }
}

vector<Card* > InitializeDeck(vector<Card* > deck)
{
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<13; j++)
        {
            Card* c = new Card((Suit)i,(Rank)j);
            deck.push_back(c);
        }
    }
    return deck;
}

void PrintForHuman(Player* player, vector<Card*> deckSuits[4], vector<Card*> LegalPlays)
{
    cout<<"Cards on the table:"<<endl;
    for(int i=0; i<4; i++)
    {
        if(i==0)
            cout<<"Clubs:";
        else if(i==1)
            cout<<"Diamonds:";
        else if(i==2)
            cout<<"Hearts:";
        else if(i==3)
            cout<<"Spades:";
        PrintMyCards(deckSuits[i]);
    }
    cout<<"Your hand:";
    PrintThisVector(player->GetCards());
    cout<<"Legal Plays:";
    PrintThisVector(player->GenerateLegalPlays(LegalPlays));
}
