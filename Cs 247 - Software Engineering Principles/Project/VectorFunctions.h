#ifndef VECTORFUNCTIONS_H
#define VECTORFUNCTIONS_H

#include <iostream>
#include <vector>
#include "Card.h"

void PrintThisVector(std::vector<Card*> givenVector);

bool SearchInVector(std::vector<Card*> givenVector, Card* card);

int GetPosition(std::vector<Card*> givenVector, Card* card);

Card* SearchInDeck(std::vector<Card*> givenVector, Card* card);

void PrintMyCards(std::vector<Card*> givenVector);

#endif
