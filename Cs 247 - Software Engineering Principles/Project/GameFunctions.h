#ifndef GAMEFUNCTIONS_H
#define GAMEFUNCTIONS_H

#include <iostream>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <algorithm>
#include "Card.h"
#include "Player.h"
#include "HumanPlayer.h"
#include "ComputerPlayer.h"

/*class Player;

class HumanPlayer:public Player;
class ComputerPlayer:public Player;*/

struct compareCards {
    bool operator()(Card *a, Card *b) { return ((a->getRank()) < (b->getRank())); }
};

void AddToCardsOnTable(Card* c, std::vector<Card*> deckSuits[4]);

std::vector<Card*> LegalMovesForASuit(std::vector<Card*> deck, std::vector<Card*> cardVector, int mySuit);

std::vector<Card* > GeneratingLegalMoves(std::vector<Card*> deck,std:: vector<Card*> deckSuits[4]);

void shuffle(std::vector<Card* > &cards_);

void InvitePlayers(Player* players[4]);

std::vector<Card* > InitializeDeck(std::vector<Card* > deck);

void PrintForHuman(Player* player, std::vector<Card*> deckSuits[4],std:: vector<Card*> legalPlays);

#endif
