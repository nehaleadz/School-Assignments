#ifndef HUMANPLAYER_H
#define HUMANPLAYER_H

#include <iostream>
#include <vector>
#include "Player.h"

class HumanPlayer:public Player
{
public:
    HumanPlayer(int score, PlayerType myType);
    class IllegalMoveException
    {
        public:
            IllegalMoveException(std::string s);
            std::string errorMessage();
        private:
            std::string errorMsg_;
    };
    Card* Play(std::vector<Card*> legalMoves, Card* card);
    void discardCard(std::vector<Card*> legalMoves, Card* card = NULL);                           //Mutator

private:
    //To check if the card is a Legal Move or not and then
    // check if the human player has it or not
    bool CheckValidity(std::vector<Card*> legalMoves, Card* card);
};

#endif
