#include <iostream>
#include <vector>
#include <memory>
#include <cstdlib>
#include <algorithm>
#include <cassert>
#include "Command.h"
#include "Card.h"
#include "VectorFunctions.h"
#include "Player.h"
#include "HumanPlayer.h"
#include "ComputerPlayer.h"
#include "GameFunctions.h"

using namespace std;

int main(int argc, char* argv[])
{
    int seed;

    if(argc>1)
    {
        seed = atoi(argv[1]);
    }
    else
    {
        seed=0;
    }
    srand48(seed);

    Player* players[4];
    vector<Card*> deck;
    vector<Card*> legalMoves;                        // The legal moves that can be made.
    vector<Card*> deckSuits[4];                      //CLUB, DIAMOND, HEART, SPADE. In that order.

    InvitePlayers(players);

    deck=InitializeDeck(deck);

    shuffle(deck);

    while(1)
    {
        //Assign Cards to the four players.
        for(int k=0, j=0; k<52; k += 13, j++){
            players[j]->AssignCards(deck, k, k+12);
        }

        legalMoves=GeneratingLegalMoves(deck, deckSuits);

        int i = 0;
        for(; i<4; i++) //Looking for the player with the seven of spades.
            if(players[i]->CheckCardExists(SEVEN, SPADE)>=0)
                break;

        //GAME BEGINS!
        cout<<"A new round begins. It\'s player "<<i+1<<"\'s turn to play."<<endl;

        for(int j = 0; j<52; j++)   //Now  each player has 13 chances, beginning with player i.
        {
            if(players[i]->getType()==HUMAN)
            {
                int flag = 0;
                PrintForHuman(players[i], deckSuits, legalMoves);
                while(!flag)
                {
                    flag = 1;
                    Command op;
                    cout<<">";
                    cin>>op;

                    try
                    {
                        Card* card = SearchInDeck(deck, &op.card);
                        switch(op.type)
                        {
                            //PLAY, DISCARD, DECK, QUIT, RAGEQUIT, BAD_COMMAND
                            case PLAY:{
                                players[i]->Play(legalMoves, card);
                                AddToCardsOnTable(card, deckSuits);              //ADD A FUNCTION TO PLACE THE CARD RETURNED CORRECTLY IN THE CHOSEN ARRAYS.
                                cout<<"Player "<<i+1<<" plays "<<*card<<"."<<endl;
                                break;
                            }
                            case DISCARD:{
                                players[i]->discardCard( legalMoves , card);
                                cout<<"Player "<<i+1<<" discards "<<*card<<"."<<endl;
                                break;
                            }
                            case DECK:{
                                for(int k=0; k<(int)deck.size(); k++)
                                {
                                    cout<<*deck[k]<<" ";
                                    if(k==12 || k==25 || k==38 ||k==51)
                                        cout<<endl;
                                }
                                flag = 0;
                                break;
                            }
                            case QUIT:{
                                exit(1);
                                break;
                            }
                            case RAGEQUIT:{
                                cout<<"Player "<<i+1<<" ragequits. A computer will now take over."<<endl;
                                Player* temp = players[i];
                                Player* newPlayer = new ComputerPlayer(temp->scoreIs(), COMPUTER);
                                vector<Card*> myCards = players[i]->GetCards();
                                newPlayer->AssignCards(myCards, 0, myCards.size()-1);
                                vector<Card*> myDiscards = players[i]->GetDiscards();
                                newPlayer->AssignDiscards(myDiscards, 0, myDiscards.size()-1);
                                delete players[i];
                                players[i] = newPlayer;
                                try
                                {
                                    Card* newC = players[i]->Play(legalMoves);
                                    cout<<"Player "<<i+1<<" plays "<<*newC<<"."<<endl;
                                    AddToCardsOnTable(newC, deckSuits);
                                }
                                catch(ComputerPlayer::NoLegalMoveException* e)  //Here because I couldn't generate any legal moves.
                                {
                                    delete e;
                                    Card* c = players[i]->GetCards()[0];
                                    players[i]->discardCard(legalMoves, c);
                                    cout<<"Player "<<i+1<<" discards "<<*c<<"."<<endl;
                                }
                                break;
                            }
                            case BAD_COMMAND:{
                                break;
                            }
                            default:
                                break;
                        }//Switch
                    }
                    catch(HumanPlayer::IllegalMoveException* e)
                    {
                        cout<<e->errorMessage()<<endl;
                        delete e;
                        flag = 0;
                    }
                }
            }
            else
            {
                try
                {
                    Card* newC = players[i]->Play(legalMoves);
                    cout<<"Player "<<i+1<<" plays "<<*newC<<"."<<endl;
                    AddToCardsOnTable(newC, deckSuits);
                }
                catch(ComputerPlayer::NoLegalMoveException* e)  //Here because I couldn't generate any legal moves.
                {
                     delete e;
                     Card* c = players[i]->GetCards()[0];
                     players[i]->discardCard(legalMoves, c);
                     cout<<"Player "<<i+1<<" discards "<<*c<<"."<<endl;
                }
            }


            i++;    //Moving on to the next player
            legalMoves=GeneratingLegalMoves(deck, deckSuits);  //  Generate the new moves
            if(i==4)    //Reset i once we hit 4.
                i=0;
        }

        //Round over. Check Scores and print the required lines.
        int maxScore = 0;
        int minScore ;      //Just pick a random player's score and compare other scores with this.
        for(int i=0; i<4; i++)
        {
            int oldScore= players[i]->scoreIs();
            cout<<"Player "<<i+1<<"\'s discards:";
            PrintThisVector(players[i]->GetDiscards());
            cout<<"Player "<<i+1<<"'s score: "<<oldScore<<" + ";
            cout<<players[i]->calculateScore()-oldScore<<" = "<<players[i]->calculateScore()<<endl;

        }
        minScore = players[0]->scoreIs();
        for(int i=1; i<4; i++){

            if(players[i]->scoreIs() >= maxScore)
                maxScore = players[i]->scoreIs();

            if(players[i]->scoreIs() <=minScore)
                minScore = players[i]->scoreIs();
        }


        //Check if someone got over an 80

        if(maxScore >= 80)
        {
             for(int i=0; i<4; i++)
                if(players[i]->scoreIs() == minScore){
                        cout<<"Player "<<i+1<<" wins!"<<endl;
                        break;
                }
            break;
        }

        //Didn't break - start anew

        shuffle(deck);
        for(int i=0; i<4; i++){
            int size = deckSuits[i].size();
            for(int j=0; j<size; j++){
                    deckSuits[i].pop_back();
            }
        }
        legalMoves=GeneratingLegalMoves(deck, deckSuits);
    }//while


    //Delete deck and players
    for(int i=0; i<(int)deck.size(); i++)
        delete deck[i];
    for(int i=0; i<4; i++)
        delete players[i];

    return 0;
}
