#include "VectorFunctions.h"

using namespace std;

void PrintThisVector(vector<Card*> givenVector)
{
    for(int i=0; i<(int)givenVector.size(); i++)
    cout<<" "<<*givenVector[i];
    cout<<endl;
}
bool SearchInVector(vector<Card*> givenVector, Card* card)
{
    for(int i=0; i<(int)givenVector.size(); i++)
        if(*givenVector[i]==*card)
            return true;
        return false;
}

int GetPosition(vector<Card*> givenVector, Card* card)
{
    for(int i=0; i<(int)givenVector.size(); i++)
        if(*givenVector[i]==*card)
            return i;
}

Card* SearchInDeck(vector<Card*> givenVector, Card* card)
{
    for(int i=0; i<(int)givenVector.size(); i++)
        if(*givenVector[i]==*card)
            return givenVector[i];
}

void PrintMyCards(vector<Card*> givenVector)
{
    string ranks[RANK_COUNT] = {"A", "2", "3", "4", "5", "6",
    "7", "8", "9", "10", "J", "Q", "K"};

    for(int i=0; i<(int)givenVector.size(); i++)
        cout <<" "<<ranks[givenVector[i]->getRank()];
    cout<<endl;
}



