#include "Player.h"
#include "VectorFunctions.h"

using namespace std;

struct Player::Impl
{
    vector<Card*> myCards_;
    PlayerType myType_;
    int score_;
    vector<Card*> myDiscards_;
};

//*******************************************
// Constructor
//*******************************************

Player::Player(int score, PlayerType myType):pimpl(new Player::Impl)
{
    pimpl->myType_ = myType;
    pimpl->score_  = score;
}

Player::~Player()
{
    //cout<<"was here";
    delete this->pimpl;
}

//*******************************************
// Accessors
//*******************************************

int Player::scoreIs()
{
    return pimpl->score_;
}

PlayerType Player::getType()
{
    return pimpl->myType_;
}

vector<Card*> Player::GetCards()
{
    return pimpl->myCards_;
}

vector<Card*> Player::GetDiscards()
{
    return pimpl->myDiscards_;
}

//*******************************************
// Mutator
//*******************************************

int Player::calculateScore()
{
    for(int i=0; i<(int)pimpl->myDiscards_.size(); i++)
    {
        int rank=(int) pimpl->myDiscards_[i]->getRank();
        pimpl->score_ += rank+1;                            //Enum values start from 0;
    }
    for(int i=(int)pimpl->myDiscards_.size();i>=1; i--)
    {
        pimpl->myDiscards_.pop_back();
    }
    return pimpl->score_;
}

//*******************************************
// Helper Functions
//*******************************************

void Player::AssignCards(vector<Card*> deck, int lowerIndex, int upperIndex)
{
    for(int i=lowerIndex;i<=upperIndex; i++)
    {
        pimpl->myCards_.push_back(deck[i]);
    }
}

void Player::AssignDiscards(vector<Card*> deck, int lowerIndex, int upperIndex)
{
    for(int i=lowerIndex;i<=upperIndex; i++)
    {
        pimpl->myDiscards_.push_back(deck[i]);
    }
}

int Player::CheckCardExists(Rank r, Suit s)
{
    for (int i=0;i<(int)pimpl->myCards_.size();i++){
        if (pimpl->myCards_[i]->getRank()==r && pimpl->myCards_[i]->getSuit()==s){
            return i;
        }
    }
    return -1;
}

vector<Card*> Player::GenerateLegalPlays(vector<Card*> legalMoves)
{
    vector <Card *> vLegalPlays;
    for (int i=0;i<(int)pimpl->myCards_.size();i++)
         for (int j=0;j<(int)legalMoves.size();j++)
              if (*pimpl->myCards_[i] == *legalMoves[j])
                vLegalPlays.push_back(pimpl->myCards_[i]);

    return vLegalPlays;
}

Card* Player::Play(vector<Card*>legalPlays, Card* card)
{

    int position = GetPosition(pimpl->myCards_, card);
    for(int i=position; i<(int)pimpl->myCards_.size()-1; i++)
        pimpl->myCards_[i]=pimpl->myCards_[i+1];
    pimpl->myCards_.pop_back();

}

void Player::discardCard(vector<Card*>legalPlays, Card* card)
{
    int position =GetPosition(pimpl->myCards_, card);
    pimpl->myDiscards_.push_back(card);

    for(int i=position; i<(int)pimpl->myCards_.size()-1; i++)
        pimpl->myCards_[i]=pimpl->myCards_[i+1];
    pimpl->myCards_.pop_back();
}

