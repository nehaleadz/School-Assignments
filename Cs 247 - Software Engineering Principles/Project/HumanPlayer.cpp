#include "HumanPlayer.h"
#include "VectorFunctions.h"

using namespace std;

//*******************************************
// Constructors
//*******************************************

HumanPlayer::HumanPlayer(int score, PlayerType myType):Player(score, myType){}

HumanPlayer::IllegalMoveException::IllegalMoveException(string s):errorMsg_(s){}

//*******************************************
// Accessors
//*******************************************

string HumanPlayer::IllegalMoveException::errorMessage(){
    return errorMsg_;
}

//*******************************************
// Helper Functions
//*******************************************

Card* HumanPlayer::Play(vector<Card*> legalMoves, Card* card)
{
    int isCardValid=CheckValidity(legalMoves, card);            //Checking validity of card.
    if (isCardValid){
        Player::Play(legalMoves, card);
    }
    else
        throw new IllegalMoveException("This is not a legal play.");
}

void HumanPlayer::discardCard(vector<Card*> legalMoves, Card* card){
    vector<Card*> legalPlays = GenerateLegalPlays(legalMoves);
    if(legalPlays.size()>0)
        throw IllegalMoveException("You have a legal play. You may not discard.");

    Player::discardCard(legalMoves, card);
}

//Two part check: 1. Is the card a LegalMove? 2. Do I have the card?
bool HumanPlayer::CheckValidity(vector<Card*> legalMoves, Card* card){
    vector<Card*> legalPlays = GenerateLegalPlays(legalMoves);

    bool isLegalMove = SearchInVector(legalMoves, card);
    bool isLegalPlay = SearchInVector(legalPlays, card);

    return (isLegalMove && isLegalPlay);

}
