#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <vector>
#include "Card.h"


enum PlayerType{COMPUTER, HUMAN};

class Player{
public:
    Player(int score, PlayerType myType);

    int calculateScore();                       //Need this for both. Discard all cards after getting the score.
    int scoreIs();
    PlayerType getType();

    ~Player();

    void AssignCards(std::vector<Card*> deck, int lowerIndex, int upperIndex);

    void AssignDiscards(std::vector<Card*> deck, int lowerIndex, int upperIndex);

    int CheckCardExists(Rank r, Suit s);       //Needed only for HumanPlayer

    virtual void discardCard(std::vector<Card*> legalMoves, Card* card = NULL);//Code to remove card from myCards_ while maintaining the order of myCards_, and add discarded card to myDiscards

    virtual Card* Play(std::vector<Card*> legalMoves, Card* card = NULL);   // Human Player send in a card, we just need to check if it's a legal move or not.
    //If not legal, throw an exception.
    //Computer Player will find the first legal move.
    //Create a legal function if needed and then call it from the main. Can access the legal vector in main.
    //Then, both functions will simply call this (the parent class's function).
    // The parent class's function, Play, will be responsible for shifting the cards back once the correct card has been found and returning the correct card.

    std::vector<Card*> GenerateLegalPlays(std::vector<Card*> legalMoves);

    //Called only by my children.
    //Looks at myCards_, compares with LegalMoves, and generates a vector. Members of this
    //vector will be legal cards, added in the SAME ORDER that they were observed in. The first legal card seen in myCards_ goes first.. so on so forth. my Cards_ will NEVER be sorted.

    std::vector<Card*> GetCards();

    std::vector<Card*> GetDiscards();

private:
    //Hide implementation.
    struct Impl;
    Impl* pimpl;
};

#endif
