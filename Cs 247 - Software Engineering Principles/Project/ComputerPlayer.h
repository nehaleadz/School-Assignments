#ifndef COMPUTERPLAYER_H
#define COMPUTERPLAYER_H

#include <iostream>
#include <vector>
#include "Player.h"

class ComputerPlayer:public Player{
public:
    ComputerPlayer(int score, PlayerType myType);
    class NoLegalMoveException
    {
        public:
            NoLegalMoveException();
    };
    Card* Play(std::vector<Card*> LegalMoves, Card* card = NULL);
};

#endif
