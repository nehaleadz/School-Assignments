#include "ComputerPlayer.h"

using namespace std;

ComputerPlayer::ComputerPlayer(int score, PlayerType myType):Player(score, myType){}

ComputerPlayer::NoLegalMoveException::NoLegalMoveException(){}

Card* ComputerPlayer::Play(vector<Card*> legalMoves, Card* card)
{
    vector<Card*> legalPlays = GenerateLegalPlays(legalMoves);
    Card* mcard;
    if(legalPlays.size()>0)
    {
        mcard = legalPlays[0];
        Player::Play(legalMoves, mcard);
    }
    else{
        throw new NoLegalMoveException();
    }
    return mcard;

}


