#include <string>
#include <iostream>
#include "Date.h"
#include <ctime>
using namespace std;

//*****************************************
// Date Declaration
//*****************************************

struct Date::Impl{
    int day_;
    std::string month_;
    int year_;
};

//*******************************************
// Constructors, Destructors, Assignment
//*******************************************

string monthsArray[12]={"January","February","March","April","May","June","July","August","September","October","November","December"};
int daysInMonth(int month) {
    switch (month){

        case 1: return 31;
        case 2: return 28;
        case 3: return 31 ;
        case 4: return 30;
        case 5: return 31;
        case 6: return 30;
        case 7: return 31;
        case 8: return 31;
        case 9: return 30;
        case 10 : return 31;
        case 11: return 30;
        case 12: return 31;
    }
        return -1;
}
int convertMonth(string monthStr) {
    if (monthStr=="January")
        return 1;
    else if (monthStr=="February")
        return 2;
    else if (monthStr=="March")
        return 3;
    else if (monthStr=="April")
        return 4;
    else if (monthStr=="May")
        return 5;
    else if (monthStr=="June")
        return 6;
    else if (monthStr=="July")
        return 7;
    else if (monthStr=="August")
        return 8;
    else if (monthStr=="September")
        return 9;
    else if (monthStr=="October")
        return 10;
    else if (monthStr=="November")
        return 11;
    else if (monthStr=="December")
        return 12;
    else
        return -1;
}
int checkLeapYear(int year){
    if (year%400 ==0 || (year%100 != 0 && year%4 == 0))
        return 1;
    else
        return 0;
}
void checkInvalidYear(int year){
      if (year<1900 || year>2100){
        throw "Invalid Year.";
    }
}
void checkInvalidMonth(std::string month){
    int validMonth=0;
    for (int i=0;i<12;i++){
        if (month==monthsArray[i]){
            validMonth=1;
            return;
        }
    }
    if (validMonth==0){
        throw "Invalid month.";
    }
}
void checkInvalidDate(int day, std::string month, int year){

    checkInvalidYear(year);
    checkInvalidMonth(month);

    int validDaysInMonth=daysInMonth(convertMonth(month));
    if (checkLeapYear(year)==1){
        if (month=="February")
            validDaysInMonth++;
    }
    if (day<1 || day>validDaysInMonth){
        throw "Invalid day of the month.";
    }
}

Date ::Date(int day, std::string month, int year):pimpl(new Date::Impl){
    checkInvalidDate(day,month,year);
    pimpl->day_=day;
    pimpl->month_=month;
    pimpl->year_=year;
}

Date::~Date(){
    delete pimpl;
}

Date::Date(const Date &d):pimpl(new Date::Impl){
    *pimpl=*d.pimpl;
}

Date &Date::operator=(const Date &d){
    if (&d==this)
        return *this;
    delete pimpl;
    pimpl=new Date::Impl(*d.pimpl);
    return *this;
}

//*******************************************
// Accessors
//*******************************************

int Date::day()const{
    return pimpl->day_;
}
std::string Date::month()const{
    return pimpl->month_;
}
int Date::year()const{
    return pimpl->year_;
}
//*******************************************
//
//*******************************************

Date Date::today(){  // current date based on current system

   time_t now = time(0);
   tm *ltm = localtime(&now);
   Date d(ltm->tm_mday,monthsArray[ltm->tm_mon],1900 + ltm->tm_year );

   return d;
}

//*******************************************
// Mutators
//*******************************************

// increment Date by num years - round down if day is invalid, return new Date
Date Date::incYears (int i) const{
    int newYear=year(),newMonth=convertMonth(month()),newDay=day();
    newYear=newYear+i;

    if (newDay>daysInMonth(newMonth)){
            newDay=daysInMonth(newMonth);
    }

    checkInvalidDate(newDay,monthsArray[newMonth-1],newYear);

    Date newDate(newDay,monthsArray[newMonth-1],newYear);


    return newDate;
}

// increment Date by num months - round down if day is invalid, return new Date
Date Date::incMonths (int i) const{
    string s=month();
    int newYear=year(),newMonth=convertMonth(month()),newDay=day();

    newMonth=newMonth+i;
    while (newMonth>=13){
        newMonth=newMonth-12;
        newYear++;
        if (newMonth<13)
            break;
    }
    if (newDay>daysInMonth(newMonth)){
            newDay=daysInMonth(newMonth);
    }

    checkInvalidDate(newDay,monthsArray[newMonth-1],newYear);

    Date newDate(newDay,monthsArray[newMonth-1],newYear);

return newDate;
}
int daysInADate(int day,int month,int year){
    int j=1,nDays=0;
    while (j<=month){
        if (checkLeapYear(year)==1 && j==2){
            if (day<daysInMonth(j) && j==month ){
                nDays=nDays+day;
            }
            else{
                nDays=nDays+daysInMonth(j)+1;
            }
        }
        else if (day<daysInMonth(j) && j==month ){
            nDays=nDays+day;
        }
        else{

            nDays=nDays+daysInMonth(j);
        }
        j++;
    }
    return nDays;
}
// increment Date by num days - round down if day is invalid, return new Date
Date Date::incDays (long i) const{
   int newYear=year(),newMonth=convertMonth(month()),newDay=day(),j=1;

   newDay=daysInADate(newDay,newMonth,newYear)+i;

   int tempDays;
   while (newDay>0){
       tempDays=newDay;
       if (checkLeapYear(newYear)==1){
            if (j==2){
                if (newDay<daysInMonth(j)){
                    newDay=newDay;
                    break;
                }
                else{
                    newDay=newDay-(daysInMonth(j)+1);
                }
            }
            else if (newDay<daysInMonth(j)){
                newDay=newDay;
                break;
            }
            else{
                newDay=newDay-daysInMonth(j);
            }
      }
      else if (newDay<=daysInMonth(j)){
            newDay=newDay;
            break;
      }
      else{
            newDay=newDay-daysInMonth(j);
       }
    j++;
    if (j>12){
        j=j-12;
        newYear++;
    }
   }
   newDay=tempDays;
   newMonth=j;
   checkInvalidDate(newDay,monthsArray[newMonth-1],newYear);
   Date newDate(newDay,monthsArray[newMonth-1],newYear);

   return newDate;
}

//*******************************************
// Streaming Functions
//*******************************************

ostream& operator<< (ostream& sout, const Date &d) {

    sout<<d.day()<<" "<<d.month()<<", "<<d.year();
    return sout;
}

istream& operator>> (istream& sin, Date &d) {
    int day,year;
    string month;
    sin>>day>>month>>year;
    month=month.substr(0,month.length()-1); //to ignore the comma at the end
    d=Date(day,month,year);

    return sin;
}
//*******************************************
// Comparison Operations
//*******************************************

bool operator== (const Date &d1, const Date &d2) {
    return ( (d1.day() == d2.day()) && (d1.month() == d2.month()) && (d1.year()==d2.year()));
}

bool operator!= (const Date &d1, const Date &d2) {
    return ( !(d1==d2) );
}

bool operator< (const Date &d1, const Date &d2){
    if (d1.year()<=d2.year()){
        if (convertMonth(d1.month())<=convertMonth(d2.month())){
            if (d1.day()<d2.day())
                return true;
        }
    }
    return false;
}
bool operator<= (const Date &d1, const Date &d2){
    return ( (d1==d2) || (d1<d2) );
}
bool operator> (const Date &d1, const Date &d2){
    return ( !(d1<=d2) );
}
bool operator>= (const Date &d1, const Date &d2){
    return ( !(d1<d2) );
}
