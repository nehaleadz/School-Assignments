#include <iostream>
#include <memory>
#include "UserAccount.h"

using namespace std;

//*****************************************
// UserAccount Definition
//*****************************************

UserAccount :: UserAccount(){
    bool validUserId=false;
    string s;
    while (validUserId!=true){
       cout<<"Enter preferred userid: ";
       try{
            cin>>s;
            userid_.reset(new Userid(s));
            validUserId=true;
       }
       catch (Userid::uniqueUseridException &e){
          cout<<e.errorMessage()<<endl;
       }
    }
    bool validPassword=false;
    string p;
    while (validPassword!=true){
            cout<<"Enter preferred password: ";
            try{
                cin>>p;
                passwd_.reset(new Password(p));
                validPassword=true;
            }
            catch (Password:: invalidPasswordException &e){
                string s=e.errorMessage(),temp="";
                int len=s.length();
                cout<<"Password :"<<endl;
                for (int i=0;i<len;i++){
                    if (s[i]!='.'){
                        temp=temp+s[i];
                    }
                    else{
                        cout<<temp<<endl;
                        temp="";
                    }
                }
            }
    }
}

void UserAccount::authenticate(){
    string sPassword;
    int noOfTries=2;
    if (check_deactivated()){
        while (noOfTries>=0 && check_deactivated()==false){
        cout<<"Enter password: ";
        cin>>sPassword;
        if (sPassword!=(*passwd_).sPassword())
        {
            if (noOfTries==2){
                cout<<"Invalid password. You have 2 more tries to get it right."<<endl;
            }
            else  if (noOfTries==1)
            {
                cout<<"Invalid password. You have 1 more try to get it right."<<endl;
            }
            else{
                cout<<"Imposter!! Account is being deactivated!!"<<endl;
                deactivate();
            }

            noOfTries--;
        }
      }
    }
    else{
        cout<<"Account has been deactivated."<<endl;
    }
}
void  UserAccount::deactivate(){
    userid_->accountActivated(false);
}

void UserAccount::reactivate(){
    userid_->accountActivated(true);
    bool validPassword=false;
    string p;
    while (validPassword!=true){
             cout<<"Enter preferred password: ";
            try{
                cin>>p;
                passwd_.reset(new Password(p));
                validPassword=true;
            }
            catch (Password:: invalidPasswordException &e){
                string s=e.errorMessage(),temp="";
                int len=s.length();
                cout<<"Password :"<<endl;
                for (int i=0;i<len;i++){
                    if (s[i]!='.'){
                        temp=temp+s[i];
                    }
                    else{
                        cout<<temp<<endl;
                        temp="";
                    }
                }
            }
    }
}

bool UserAccount::check_deactivated()const{
     return userid_->isAccountActivated();
}
