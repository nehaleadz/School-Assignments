
#ifndef PASSWORD_H
#define PASSWORD_H

#include <iostream>
using namespace std;
class Password {
public:
    class invalidPasswordException{
    public:
        invalidPasswordException(string s);
        string errorMessage();
    protected:
        void errorMsgIs(string s);
    private:
        string errorMsg_;
    };
    Password (string sPassword);  // Constructor for user-provided values
    Password &operator= (const Password &);          // Assignment
    string sPassword()const;                         // Accessor
protected:
     void isValidPasssword(string s);
private:
    string sPassword_;
};

#endif

