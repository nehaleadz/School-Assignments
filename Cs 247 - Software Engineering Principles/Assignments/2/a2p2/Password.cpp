#include <iostream>
#include <memory>
#include "Password.h"

using namespace std;

//*******************************************
// Helper function
//*******************************************

void Password::isValidPasssword(string s){
    string sError="";
    if (s.length()<8){
        sError=sError+"    - must be at least 8 characters.";
    }
    int nUpperCase=0,nDigits=0,nSymbol=0;
    for (int i=0;i<s.length();i++){
        if (int(s[i])>=65 && int(s[i])<=90) // A - Z
            nUpperCase++;
         else if (int(s[i])>=48 && int(s[i])<=57)  // 0 -9
            nDigits++;
        else if (s[i]=='~' || s[i]=='!' || s[i]=='@' || s[i]=='#' || s[i]=='$' ||
                  s[i]=='%' || s[i]=='^' || s[i]=='&' || s[i]=='*' || s[i]=='(' ||
                  s[i]==')' || s[i]=='_' || s[i]=='-' || s[i]=='+' || s[i]=='=' ||
                  s[i]=='{' || s[i]=='}' || s[i]=='[' || s[i]==']' || s[i]==':' ||
                  s[i]==';' || s[i]=='<' || s[i]=='>' || s[i]=='.' || s[i]=='?' || s[i]=='/')
            nSymbol++;
    }

    if (nUpperCase<2){
        sError=sError+"    - must include at least 2 capital letters.";
    }
   if (nDigits<2){
        sError=sError+"    - must include at least 2 digits.";
    }
    if (nSymbol <1){
       sError=sError+"    - must include at least 1 symbol.";
    }
    if (sError!=""){
        throw invalidPasswordException(sError);
    }
}

//*******************************************
// Constructor
//*******************************************

Password :: Password (string sPassword) {
   isValidPasssword(sPassword) ;
   sPassword_=sPassword;
}
Password::invalidPasswordException::invalidPasswordException(string s):errorMsg_(s){
}

//*******************************************
// Accessors
//*******************************************

string Password:: sPassword()const{
    return sPassword_;
}

string Password::invalidPasswordException::errorMessage(){
    return errorMsg_;
}

//*******************************************
// Mutator
//*******************************************

void Password::invalidPasswordException::errorMsgIs(string s){
    errorMsg_=s;
}
