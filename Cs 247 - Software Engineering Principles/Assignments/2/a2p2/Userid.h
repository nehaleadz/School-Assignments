
#ifndef USERID_H
#define USERID_H

#include <iostream>
#include <vector>
using namespace std;
class Userid {
public:
    class uniqueUseridException{
    public:
        uniqueUseridException(string s);
        string errorMessage();
    protected:
        void errorMsgIs(string s);
    private:
        string errorMsg_;
    };
    Userid (string s);                                       // Constructor for user-provided values
    ~Userid ();                                              // Destructor
    Userid &operator= (const Userid &uId);
    string sUserid()const;                                   // Accessor
    vector <string> accountExists()const;                    // Accessor
    bool isAccountActivated() const;                         // Accessor
    void accountActivated(bool status);                      // Mutator

protected:
    void accountIs(string s);                                // Mutator
private:
    string sUserid_;
    static vector <string> accountExists_;                   // vector to store all userids
    bool isAccountActivated_;                                // whether userid is activated or not

};

#endif
