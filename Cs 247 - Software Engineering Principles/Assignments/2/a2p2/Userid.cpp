#include <iostream>
#include <memory>
#include "Userid.h"

using namespace std;

vector <string> Userid::accountExists_;

//*******************************************
// Constructors, Destructors, Assignment
//*******************************************
Userid::uniqueUseridException::uniqueUseridException(string s):errorMsg_(s){

}
Userid:: Userid (string sUserid){
    bool doesAccountExist=false;
    vector <string>temp;
    temp=accountExists();
    int vectorSize=temp.size();
    for (int i=0; i<vectorSize; i++)
    {
        if (sUserid==accountExists()[i])
        {
            doesAccountExist=true;
            break;
        }
    }
    if (doesAccountExist)
        throw uniqueUseridException("Userid \""+sUserid+"\" already exists. Please try again.");
    sUserid_=sUserid;
    accountIs(sUserid);
}
Userid::~Userid() {
    int vecSize=accountExists_.size();
    for (int i=0;i<vecSize;i++){
        if (accountExists_[i]==sUserid_){
            accountExists_.erase(accountExists_.begin()+i);
            break;
        }
    }
}
Userid & Userid:: operator= (const Userid &uId){
    if (&uId==this)
        return *this;
    sUserid_=uId.sUserid_;
    return *this;
}

//*******************************************
// Accessors
//*******************************************

string Userid:: sUserid()const
{
    return sUserid_;
}

vector <string> Userid:: accountExists()const
{
    return accountExists_;
}

bool Userid:: isAccountActivated() const
{
    return isAccountActivated_;
}

string Userid::uniqueUseridException:: errorMessage()
{
    return errorMsg_;
}

//*******************************************
// Mutators
//*******************************************

void Userid::accountIs(string s)
{
    accountExists_.push_back(s);
}

void Userid:: accountActivated(bool status){
        isAccountActivated_=status;
}
