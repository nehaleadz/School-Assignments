#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <cctype>

using namespace std;
bool containsAlphabets(string s){
    for (int i=0;i<(int)s.length();i++){
        if (isalpha(s[i])){}
        else
            return false;
    }
}
bool is_empty(ifstream& pFile){
    return pFile.peek() == std::ifstream::traits_type::eof();
}
int main(int argc,char* argv[]){

    ifstream iFile;
    string str;
    vector <string>v;

    if (argv[2]==NULL)
        srand48(0);
    else{
        int seed=atoi(argv[2]);
        srand48(seed);
    }

    if (argv[1]==NULL){
        cout<<"Error: No input file specified.";
    }
    else
        iFile.open(argv[1]);

/************ POOL OF GAMEWORDS ***************************/

    if (!iFile.is_open())
        cout <<"Error: Could not open file "<<argv[1]<<".";
    else{
        while (iFile>>str){
        if (str.length()>=6 && containsAlphabets(str))
            v.push_back(str);
        }
        if (v.size()==0 || is_empty(iFile)){
            cout<<"Error: Pool of game words is empty.";
        }

        ofstream oFile;
        oFile.open ("gamewords");
        for ( int i = 0; i <(int) v.size(); i++ )
            oFile << v[i] << "\n";
        oFile.close();
    }

/************ START GAME ***************************/

    int guessIndex=lrand48()%v.size(),nLives=5;
    string gameWord=v[guessIndex];
    string printGameWord="";
    for (int k=0;k<(int)gameWord.length();k++)
        printGameWord=printGameWord+"-";

    cout <<"Word: "+printGameWord<<endl;
    cout <<"Letters used:"<<endl;
    cout <<"You have "<<nLives<<" lives left."<<endl;
    cout <<"Next guess: ";

/************ PLAY GAME ***************************/

    vector <string>guessLetters;
    return 0;
}
