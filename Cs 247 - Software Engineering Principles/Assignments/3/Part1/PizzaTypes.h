
#ifndef PIZZA_TYPES__
#define PIZZA_TYPES__

#include "Pizza.h"

class Vegetarian:Pizza{
public:
    virtual float cost()const;
    virtual std::string description()const;
};

class NewYorker:Pizza{
public:
    virtual float cost()const;
    virtual std::string description()const;
};

class Pepperoni:Pizza{
public:
    virtual float cost()const;
    virtual std::string description()const;
};

#endif
