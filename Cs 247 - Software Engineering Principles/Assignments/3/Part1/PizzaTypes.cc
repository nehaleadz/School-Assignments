#include <string>
#include <iostream>
#include "Vegetarian.h"
using namespace std;

float Vegetarian::cost()const{
    return 10.99;
}

string Vegetarian::description()const(){
    return "Vegetarian";
}

float NewYorker::cost()const{
    return 9.99;
}

string NewYorker::description()const{
    return "New Yorker";
}

float Pepperoni::cost()const{
    return 8.99;
}

string Pepperoni::description()const{
    return "Pepperoni";
}
