#include <string>
#include "ToppingsDecorator.h"
using namespace std;

ToppingsDecorator::ToppingsDecorator(const Pizza p):pizza_(p){}

const Pizza ToppingsDecorator::pizzaIs()const{
    return pizza_;
}

/*****************************
 BLACK OLIVES
*******************************/
BlackOlives::BlackOlives (const Pizza p): ToppingsDecorator(p){}

float BlackOlives::cost()const{
    return 0.99+pizzaIs().cost();
}
string BlackOlives::description()const{
    return pizzaIs().description()+", Black Olives";
}


/*****************************
    RED PEPPER
*******************************/

RedPepper::RedPepper (const Pizza p): ToppingsDecorator(p){}

float RedPepper::cost()const{
    return 0.97+pizzaIs().cost();
}
string RedPepper::description()const{
    return pizzaIs().description()+", Red Pepper";
}

/*****************************
 SUN-DRIED TOMATO
*******************************/
SunDriedTomatoes::SunDriedTomatoes(const Pizza p): ToppingsDecorator(p){}

float SunDriedTomatoes::cost()const{
    return 0.98+pizzaIs().cost();
}
string SunDriedTomatoes::description()const{
    return pizzaIs().description()+", SunDried Tomatoes";
}
