
#include "Pizza.h"
#include "ToppingsDecorator"
#include "PizzTypes"
#include <iostream>
#include <string>

using namespace std;
void addTopings(Pizza &p){
    char input;

    cout<<"Do you want to add topings? [Y/N]: ";
    cin>>input;

    while (input=='Y' || input=='y'){
        cout<<endl<<"Select the following toppings: (one at a time)"<<endl;
        cout<<"1. Black Olives "<<endl;
        cout<<"2. Red Pepper "<<endl;
        cout<<"3. Sundried Tomatoes "<<endl;

         int option;
         cin>>option;

        switch(option){
            case 1:{
                p=new BlackOlives(p);
                break;
            }
            case 2:{
                p=new RedPepper(p);
                break;
            }
            case 3:{
                p=new SunDriedTomatoes(p);
                break;
            }
            default:{
                cout<<"Enter a correct option! "<<endl;
                break;
            }
        }
        cout<<"Do you want to add topings? [Y/N]: ";
        cin>>input;
    }
}
int main(int argc char* argv[]){

    cout<<"Welcome to Popular pizza !!! "<<endl<<endl;
    cout<<"Select a pizza  "<<endl;
    cout<<"\t1. Vegetarian "<<endl;
    cout<<"\t2. New Yorker "<<endl;
    cout<<"\t3. Pepperoni "<<endl;

    int option;
    cin>>option;

    switch(option){
        case 1:{
            Pizza p=new Vegetarian();
            addTopings(Pizza );
            break;
        }
        case 2:{
            Pizza p=new NewYorker();
            break;
        }
        case 3:{
            Pizza p=new Pepperoni();
            break;
        }
        default:{
            cout<<"Enter a correct option! "<<endl;
            break;
        }
    }
    addTopings(&p);
    cout<<endl<<p.description()<<endl;
    cout<<endl<<"Bill: "<<p.cost()<<endl;
    return 0;
}
