#include "Vegetarian.h"

Vegetarian::Vegetarian(const Pizza *p):pizza_(p){}

float Vegetarian::cost()const{
    return 12.5;
}

std::string Vegetarian::description()const(){
    return "Vegetarian";
}
