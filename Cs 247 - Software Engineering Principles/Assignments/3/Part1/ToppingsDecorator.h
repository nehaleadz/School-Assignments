
#ifndef TOPPINGS_DECORATOR__
#define TOPPINGS_DECORATOR__

#include "Pizza.h"

class ToppingsDecorator:public Pizza{
    const Pizza pizza_;
protected:
    ToppingsDecorator(const Pizza*);
    const Pizza pizzaIs()const;
};

class BlackOlives:public ToppingsDecorator{
public:
    BlackOlives(const Pizza);
    virtual float cost()const;
    virtual std::string description()const;
};

class RedPepper:public ToppingsDecorator{
public:
    RedPepper(const Pizza);
    virtual float cost()const;
    virtual std::string description()const;
};

class SunDriedTomatoes:public ToppingsDecorator{
public:
    SunDriedTomatoes(const Pizza);
    virtual float cost()const;
    virtual std::string description()const;
};


#endif

