
#ifndef VEGETARIAN__
#define VEGETARIAN__

#include "Pizza.h"

class Vegetarian:Pizza{
public:
    virtual float cost()const;
    virtual std::string description()const;
}
