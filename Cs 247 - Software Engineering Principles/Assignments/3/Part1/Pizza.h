#ifndef PIZZA__
#define PIZZA__

class Pizza{
public:
    virtual ~Pizza(){}
    virtual float cost()const()=0;
    virtual std::string description() const=0;
};

#endif
