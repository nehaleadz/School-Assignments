#include "stm32f4xx.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
/*
 * Main Function (program point of entry)
 */
int timeElapsed=0;
int gameStarted=0;
int timeRemaning=0;
int potatoDelay=150;

void TIM7_IRQHandler(void)
{
    //Check interrupt was triggered by update event of TIMn
    if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET)
    {
        timeElapsed++;
        
        if (timeElapsed>1000) //this means 10 second
	    {
	    	timeElapsed=0;
	    	TIM_Cmd(TIM7,DISABLE);
	    	gameStarted=0;
	    }
        potatoDelay--;
        
        if (potatoDelay<0)
	    {
    		printf("potatoDelay %d,TimeElapsed: %d\n", potatoDelay, timeElapsed);
    		
    		//Send rising edge on GPIO port A Pin 1 i.e. set pin 1 to 1 
			GPIOA->ODR|=(0x1<<1);
			//Reset pin 1 on GPIO port A to 0
			GPIOA->ODR&=~(0x1<<1);
			potatoDelay=150;
	    }
        else if (timeElapsed>970 && potatoDelay<0)
        {
        	printf("Last rising edge: potatoDelay %d,TimeElapsed: %d\n", potatoDelay, timeElapsed);
        	
        	//Send rising edge on GPIO port A Pin 1 i.e. set pin 1 to 1 
			GPIOA->ODR|=(0x1<<1);
			//Reset pin 1 on GPIO port A to 0
			GPIOA->ODR&=~(0x1<<1);
			potatoDelay=150;
        }
        
        //Clear the interrupt pending flag for timer update
        TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
    }
} 
	
void EXTI0_IRQHandler(void)
{
    if (gameStarted==0)
    {
    	//Start timer
		TIM_Cmd(TIM7,ENABLE);
		gameStarted=1;
    }
    if (timeElapsed<=640)
    {
    	potatoDelay=150;
    }
    else if (timeElapsed>640 && timeElapsed<690)
    {
    	potatoDelay=60;
    }
    else if (timeElapsed>=700 && timeElapsed<800)
    {
    	potatoDelay=199;
    }
    else if (timeElapsed>=800 && timeElapsed<850)
    {
    	potatoDelay=149;
    }
    else if (timeElapsed>=850 && timeElapsed<900)
    {
    	potatoDelay=99;
    }
    else if (timeElapsed>=900 && timeElapsed<950)
    {
    	potatoDelay=51;
    }
    
    //Write a 1 to bit i of EXTI_PR
    //This clears interrupt pending flag for EXTI line i
    EXTI->PR |= (0x1<<0);
}

int main(void)
{
	//Enable AHB1 peripheral clock for the GPIO port B
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	
	//Configure GPIO port B pin 0 to input mode
	GPIO_InitTypeDef GPIO_InitStructure_B;
	GPIO_InitStructure_B.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure_B.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure_B.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOB, &GPIO_InitStructure_B);
	
	//Enable AHB1 peripheral clock for the GPIO port A
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	
	//Configure GPIO port A pin 1 to output mode
	GPIO_InitTypeDef GPIO_InitStructure_A;
	GPIO_InitStructure_A.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure_A.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure_A.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure_A.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure_A.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOA, &GPIO_InitStructure_A);
	
	//Enable APB1 Peripheral Clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
	
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_TimeBaseStructure.TIM_Period = 840 - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 1000 - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);
	
	//Enable timer interrupts on update events for timer TIM7
	TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);
		
	//Configure Nested Vectred Interrupt Controller
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	//Enable SysCfg Clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG,ENABLE);
	//connect EXTI line i to GPIO port x pin i
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource0);
	
	//Configuring EXTI line 0 on rising edges
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	
	//Configure NVIC for an EXTI Line
	NVIC_InitTypeDef NVIC_InitStructureEXTI;
	NVIC_InitStructureEXTI.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStructureEXTI.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructureEXTI.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructureEXTI.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructureEXTI);
	
	//enable the EXTI line 0 interrupt
	EXTI->IMR |= (0x1<<0);
	
	while(1);
	
	return 0;
}

