#include "stm32f4xx.h"
#include <stdio.h>

/*  Bursty Software Scheduler
1. Use a counter to count interrupts
2. After burst completed, force the delay with the timer

1. Disable an interrupt after a burst of interrupt requests 
	have been observed. 
2. In the code for handling interrupt, increment a counter and, 
   if the counter is greater than or equal to the burst size, 
   clear the interrupt’s enable bit to avoid overload.    
3. The interrupt counter is cleared by a periodic timer that runs 
   asynchronously with respect to the interrupt workload;
4. The frequency of this timer is the burst arrival rate. This timer also
   sets the interrupt enable bit if it was previously cleared.	
5. A useful performance optimization is to leave the periodic timer
   interrupt disabled as long as the counter is below the threshold,
   avoiding timer overhead in the expected case where the threshold
   is seldom exceeded. 
6. This only works when a dedicated hardware timer is available for the 
   interrupt scheduler; during underload its effect is to leave the timer 
   interrupt pending almost all of the time. It is also necessary to clear the
   timer interrupt’s pending bit just before enabling the timer interrupt, 
   in order to avoid a bad case where three back-to-back bursts of device 
   interrupts could otherwise occur

*/
int burstCounter=0;
int maxBurstCounter=5;

void EXTI0_IRQHandler(void)
{
	burstCounter+=1;
	if (burstCounter>=maxBurstCounter)
	{
		//Clearing the interrupt's enable bit
		//disable the EXTI line 0 interrupt
		EXTI->IMR &= ~(0x1<<0);
	}

    //Start timer
	TIM_Cmd(TIM7,ENABLE);
	
    //Send a rising edge to port A pin 1
	GPIOA->ODR|=(0x1<<1);
	//Reset the port A  pin 1 to 0
	GPIOA->ODR&=~(0x1<<1);
	
	//This clears interrupt pending flag for EXTI line 0
    EXTI->PR |= (0x1<<0);
}
void TIM7_IRQHandler(void)
{	
	//Check interrupt was triggered by update event of TIMn
    if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET)
    {
        //Stop timer
		TIM_Cmd(TIM7,DISABLE);
		
		burstCounter=0;
		
        //enable the EXTI line 0 interrupt
		EXTI->IMR |= (0x1<<0);
				
        //Clear the interrupt pending flag for timer update
        TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
    }
    
}
int main(void)
{
	// Enable the peripheral clock for GPIO port A and port B
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	
	//Configure GPIO port A pin 1 to output mode
	GPIO_InitTypeDef GPIO_InitStructureA;
	GPIO_InitStructureA.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructureA.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructureA.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructureA.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructureA.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOA, &GPIO_InitStructureA);
	
	//Configure GPIO port B pin 0 to input mode
	GPIO_InitTypeDef GPIO_InitStructureB;
	GPIO_InitStructureB.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructureB.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructureB.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOB, &GPIO_InitStructureB);
	
	/*** Setting up EXTI timers ****/
	//Enable SysCfg Clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG,ENABLE);
	//connect EXTI line 0 to GPIO port B pin 0
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource0);
	
	//Configure EXTI line 0 on rising edges
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	
	//Configure NVIC for an EXTI line
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	//enable the EXTI line 0 interrupt
	EXTI->IMR |= (0x1<<0);
	
	//disable the EXTI line 0 interrupt
	//EXTI->IMR &= ~(0x1<<0);
	/**************************************/
	
	/*** Setting up Hardware timers ****/
	//Enable APB1 Peripheral Clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
	
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_TimeBaseStructure.TIM_Period = 20580 - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 1000 - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);
	
	//Enable timer interrupts on update events for timer TIM7
	TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);
	
	//Configure Nested Vectred Interrupt Controller
	NVIC_InitTypeDef NVIC_InitStructure7;
	NVIC_InitStructure7.NVIC_IRQChannel = TIM7_IRQn;
	NVIC_InitStructure7.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure7.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure7.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure7);
	/**************************************/
	
	while(1);
	
	return 0;
}

