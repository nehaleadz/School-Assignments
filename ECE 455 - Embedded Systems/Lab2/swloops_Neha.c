#include "stm32f4xx.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
/*
 * Main Function (program point of entry)
 */
 
void delayLoop()
{
   int c;
   for ( c = 0 ; c < 120000 ; c++ ){}
}
int main(void)
{
	
	//Enable AHB1 peripheral clock for the GPIO port A
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	
	//Set GPIO port A pin 1 to output mode
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//Set pin 1 on GPIO port A to 0
	GPIOA->ODR&=~(0x1<<1);
	
	while(1)
	{
		//delay for 10ms
		delayLoop();
		
		//Send rising edge on GPIO port A Pin 1 i.e. set pin 1 to 1 
		GPIOA->ODR|=(0x1<<1);
		
		//Reset pin 1 on GPIO port A to 0
		GPIOA->ODR&=~(0x1<<1);

	}
	return 0;
}

