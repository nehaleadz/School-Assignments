(function($) {
    
$(document).ready(function() {
    if ($.browser.msie || $.browser.opera || ($.browser.safari && (/Apple/i.test(navigator.vendor)) && !(/536./.test($.browser.version))) ) {
        $('#browsercheck').show();
    }
});

})(jQuery);