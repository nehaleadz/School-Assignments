(function($) {

getState = function() {
    $("#loader").show();
    $.ajax("/workspace/_get/state")
        .done(function(res) {
            var nextCheck = 3;
            
            if (res.state != Drupal.settings.workspace.state) {
                Drupal.settings.workspace.state = res.state;
            }
            else {
                switch (res.state) {
                    case 'none':
                        Drupal.settings.workspace.available = res.spaces != 0;
                    case 'occupied':
                        nextCheck = 60;
                        $('#loader').hide();
                    default:
                        break;
                }
            }
            
            updateLabListAction();
            updateState = setTimeout(getState, nextCheck * 1000);
            
        });
}

$(document).ready(function() {

    updateLabListAction();
    updateState = setTimeout(getState, 1 * 1000);
    
});

})(jQuery);
