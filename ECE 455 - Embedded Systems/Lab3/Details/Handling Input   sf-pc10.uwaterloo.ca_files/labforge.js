(function($) {

updateLabListAction = function() {

    $('.start-lab', '#main-content').each(function(index, Element) {
        var $content;
        if (Drupal.settings.workspace.state != 'none') {
            $content = $('<span>')
                .addClass('btn disabled')
                .text('Lab in progress')
                .attr('title', 'You will need to end your current lab session before starting a new one.');
        }
        else if (!Drupal.settings.workspace.available) {
            $content = $('<span>')
                .addClass('btn disabled')
                .text('No lab spaces available');
        }
        else {
            var nid = $(Element).attr('nid');
            $content = $('<a>')
                .text('Start lab')
                .addClass('btn btn-success')
                .attr('href', '/workspace/init/' + nid);    
        }
        $(Element).html($content);
    });
    
}

})(jQuery);