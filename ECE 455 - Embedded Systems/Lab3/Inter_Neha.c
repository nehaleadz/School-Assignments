#include "stm32f4xx.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
/*
 * Main Function (program point of entry)
 */
void TIM7_IRQHandler(void)
{
    //Check interrupt was triggered by update event of TIMn
    if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET)
    {
    	//Send rising edge on GPIO port A Pin 1 i.e. set pin 1 to 1 
		GPIOA->ODR|=(0x1<<1);
        
        //Reset pin 1 on GPIO port A to 0
		GPIOA->ODR&=~(0x1<<1);
        
        //Clear the interrupt pending flag for timer update
        TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
    }
} 
 
int main(void)
{
	
	//Enable APB1 peripheral clock for Tim7
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
	
	//Setup a basic count timer
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_TimeBaseStructure.TIM_Period = 840 - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 1000 - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);
	
	//Enable AHB1 peripheral clock for the GPIO port B
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	
	//Configure GPIO port B pin 0 to input mode
	GPIO_InitTypeDef GPIO_InitStructure_B;
	GPIO_InitStructure_B.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure_B.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure_B.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOB, &GPIO_InitStructure_B);
	
	//Enable AHB1 peripheral clock for the GPIO port A
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	
	//Configure GPIO port A pin 1 to output mode
	GPIO_InitTypeDef GPIO_InitStructure_A;
	GPIO_InitStructure_A.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure_A.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure_A.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure_A.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure_A.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOA, &GPIO_InitStructure_A);
	
	//Start timer
	TIM_Cmd(TIM7,ENABLE);
	
	//Enable timer interrupts on update events for timer TIM7
	TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);
	
	//Configure Nested Vectred Interrupt Controller
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	while(1);
	
	return 0;
}

