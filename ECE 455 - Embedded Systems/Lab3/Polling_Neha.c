#include "stm32f4xx.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
/*
 * Main Function (program point of entry)
 */

int main(void)
{
	//Enable AHB1 peripheral clock for the GPIO port B
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	
	//Configure GPIO port B pin 0 to input mode
	GPIO_InitTypeDef GPIO_InitStructure_B;
	GPIO_InitStructure_B.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure_B.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure_B.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOB, &GPIO_InitStructure_B);
	
	//Enable AHB1 peripheral clock for the GPIO port A
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	
	//Configure GPIO port A pin 1 to output mode
	GPIO_InitTypeDef GPIO_InitStructure_A;
	GPIO_InitStructure_A.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure_A.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure_A.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure_A.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure_A.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOA, &GPIO_InitStructure_A);
	
	while(1)
	{
		if (GPIOB->IDR & (0x1<<0))
		{
			//Send rising edge on GPIO port A Pin 1 i.e. set pin 1 to 1 
			GPIOA->ODR|=(0x1<<1);
			
			//Reset pin 1 on GPIO port A to 0
			GPIOA->ODR&=~(0x1<<1);
		}

	}
	return 0;
}

