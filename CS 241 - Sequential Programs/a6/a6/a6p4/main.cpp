/*  CS241 Scanner

    Starter code for the CS 241 assembler (assignments 3 and 4).
    Code contained here may be included in submissions for CS241
    assignments at the University of Waterloo.

    ---------------------------------------------------------------

    To compile on a CSCF linux machine, use:

            g++ -g asm.cc -o asm

    To run:
            ./asm           < source.asm > program.mips
            valgrind ./asm  < source.asm > program.mips
 */

#include <string>
#include <vector>
#include <iostream>
#include <cstdio>
using namespace std;

//======================================================================
//========= Declarations for the scan() function =======================
//======================================================================

// Each token has one of the following kinds.

enum Kind {
    ID,
    NUM,
    LPAREN,
    RPAREN,
    LBRACE,
    RBRACE,
    BECOMES,
    EQ,
    LT,
    GT,
    LE,
    GE,
    PLUS,
    MINUS,
    STAR,
    SLASH,
    PCT,
    COMMA,
    SEMI,
    LBRACK,
    RBRACK,
    AMP,
    WHITESPACE,
    NOT,
    NOTEQ,
    RETURN,
    IF,
    ELSE,
    WHILE,
    PRINTLN,
    WAIN,
    NEW,
    DELETE,
    INT,
    NUL,
};

// kindString(k) returns string a representation of kind k
// that is useful for error and debugging messages.
string kindString(Kind k);

// Each token is described by its kind and its lexeme.

struct Token {
    Kind      kind;
    string    lexeme;
    /* toInt() returns an integer representation of the token. For tokens
     * of kind INT (decimal integer constant) and HEXINT (hexadecimal integer
     * constant), returns the integer constant. For tokens of kind
     * REGISTER, returns the register number.
     */
    int       toInt();
};

// scan() separates an input line into a vector of Tokens.
vector<Token> scan(string input);

// =====================================================================
// The implementation of scan() and associated type definitions.
// If you just want to use the scanner, skip to the next ==== separator.

// States for the finite-state automaton that comprises the scanner.

enum State {
    ST_ID,
    ST_NUM,
    ST_LPAREN,
    ST_RPAREN,
    ST_LBRACE,
    ST_RBRACE,
    ST_BECOMES,  // =
    ST_EQ,      // ==
    ST_LT,
    ST_GT,
    ST_GE,
    ST_LE,
    ST_PLUS,
    ST_MINUS,
    ST_STAR,
    ST_SLASH,
    ST_PCT,
    ST_COMMA,
    ST_SEMI,
    ST_LBRACK,
    ST_RBRACK,
    ST_AMP,
    ST_NUL,
    ST_NOT,
    ST_NOTEQ,
    ST_START,
    ST_COMMENT,
    ST_WHITESPACE
};

// The *kind* of token (see previous enum declaration)
// represented by each state; states that don't represent
// a token have stateKinds == NUL.

Kind stateKinds[] = {
    ID,
    NUM,
    LPAREN,
    RPAREN,
    LBRACE,
    RBRACE,
    BECOMES,
    EQ,
    LT,
    GT,
    GE,
    LE,
    PLUS,
    MINUS,
    STAR,
    SLASH,
    PCT,
    COMMA,
    SEMI,
    LBRACK,
    RBRACK,
    AMP,
    NUL,
    NUL,
    NOTEQ,
    NUL,
    WHITESPACE,
    WHITESPACE
};

State delta[ST_WHITESPACE+1][256];
#define whitespace "\t\n\r "
#define letters    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define digits     "0123456789"
#define oneToNine  "123456789"

void setT(State from, string chars, State to) {
    for(int i = 0; i < chars.length(); i++ ) delta[from][chars[i]] = to;
}

void initT(){
    int i, j;

    // The default transition is ST_NUL (i.e., no transition
    // defined for this char).
    for ( i=0; i<=ST_WHITESPACE; i++ ) {
        for ( j=0; j<256; j++ ) {
            delta[i][j] = ST_NUL;
        }
    }
    // Non-null transitions of the finite state machine.
    // NB: in the third line below, letters digits are macros
    // that are replaced by string literals, which the compiler
    // will concatenate into a single string literal.
    setT( ST_START,      whitespace,     ST_WHITESPACE );
    setT( ST_WHITESPACE, whitespace,     ST_WHITESPACE );
    setT( ST_START,      letters       , ST_ID         );
    setT( ST_ID,         letters digits, ST_ID         );
    setT( ST_NUM  ,      digits,         ST_NUM        );
    setT( ST_START,      "0"            ,ST_NUM        );
    setT( ST_START,      oneToNine,      ST_NUM        );
    setT( ST_START,      "(",            ST_LPAREN     );
    setT( ST_START,      ")",            ST_RPAREN     );
    setT( ST_START,      "{",            ST_LBRACE     );
    setT( ST_START,      "}",            ST_RBRACE     );
    setT( ST_START,      "=",            ST_BECOMES     );
    setT( ST_START,      "+",            ST_PLUS       );
    setT( ST_START,      "-",            ST_MINUS      );
    setT( ST_START,      "*",            ST_STAR       );
    setT( ST_START,      "<",            ST_LT         );
    setT( ST_START,      ">",            ST_GT         );
    setT( ST_START,      "/",            ST_SLASH      );
    setT( ST_SLASH,      "/",            ST_COMMENT    );
    setT( ST_START,      "%",            ST_PCT        );
    setT( ST_START,      ",",            ST_COMMA      );
    setT( ST_START,      ";",            ST_SEMI       );
    setT( ST_START,      "[",            ST_LBRACK     );
    setT( ST_START,      "]",            ST_RBRACK     );
    setT( ST_START,      "&",            ST_AMP        );
    setT( ST_START,      "!",            ST_NOT        );
    setT( ST_GT   ,      "=",            ST_GE         );
    setT( ST_LT   ,      "=",            ST_LE         );
    setT( ST_NOT  ,      "=",            ST_NOTEQ      );

    for ( j=0; j<256; j++ ) delta[ST_COMMENT][j] = ST_COMMENT;
}

static int initT_done = 0;

vector<Token> scan(string input){
    // Initialize the transition table when called for the first time.
    if(!initT_done) {
        initT();
        initT_done = 1;
    }

    vector<Token> ret;

    int i = 0;
    int startIndex = 0;
    State state = ST_START;

    if(input.length() > 0) {
        while(true) {
            State nextState = ST_NUL;

            if(i < input.length())
                nextState = delta[state][(unsigned char) input[i]];

            if(nextState == ST_NUL) {
                // no more transitions possible
                if(stateKinds[state] == NUL) {
                    throw("ERROR in lexing after reading " + input.substr(0, i));
                }
                if(stateKinds[state] != WHITESPACE) {
                    Token token;
                    token.kind = stateKinds[state];
                    token.lexeme = input.substr(startIndex, i-startIndex);

                    ret.push_back(token);
                }
                startIndex = i;
                state = ST_START;
                if(i >= input.length()) break;
            } else {
                state = nextState;
                i++;
            }
        }
    }
    return ret;
}

// kindString maps each kind to a string for use in error messages.

string kS[] = {
    "ID",
    "NUM",
    "LPAREN",
    "RPAREN",
    "LBRACE",
    "RBRACE",
    "BECOMES",
    "EQ",
    "LT",
    "GT",
    "LE",
    "GE",
    "PLUS",
    "MINUS",
    "STAR",
    "SLASH",
    "PCT",
    "COMMA",
    "SEMI",
    "LBRACK",
    "RBRACK",
    "AMP",
    "WHITESPACE",
    "NOT",
    "NOTEQ",
    "RETURN",
    "IF",
    "ELSE",
    "WHILE",
    "PRINTLN",
    "WAIN",
    "NEW",
    "DELETE",
    "INT",
    "NUL"
};

string kindString( Kind k ){
    if ( k < ID || k > NUL )
        return "INVALID";
    return kS[k];
}

//======================================================================
//======= A sample program demonstrating the use of the scanner. =======
//======================================================================

int main() {

    try {
        vector<string> srcLines;

        // Read the entire input file, storing each line as a
        // single string in the array srcLines.
        while(true) {
            string line;
            getline(cin, line);
            if(cin.fail()) break;
            srcLines.push_back(line);
        }

        // Tokenize each line, storing the results in tokLines.
        vector<vector<Token> > tokLines;

        for(int line = 0; line < srcLines.size(); line++) {
            tokLines.push_back(scan(srcLines[line]));
        }

        // Now we process the tokens.
        // Sample usage: print the tokens of each line.
        for(int line=0; line < tokLines.size(); line++ ) {
            for(int j=0; j < tokLines[line].size(); j++ ) {
                Token token = tokLines[line][j];
                Token nextToken;

                if (tokLines[line].size()==(j+1))
                    nextToken = tokLines[line][j];
                else
                    nextToken = tokLines[line][j+1];

                    if (token.lexeme=="return")
                        token.kind = RETURN;
                    else if (token.lexeme=="if")
                        token.kind = IF;
                    else if (token.lexeme=="else")
                        token.kind = ELSE;
                    else if (token.lexeme=="while")
                        token.kind = WHILE;
                    else if (token.lexeme=="println")
                        token.kind = PRINTLN;
                    else if (token.lexeme=="wain")
                        token.kind = WAIN;
                    else if (token.lexeme=="new")
                        token.kind = NEW;
                    else if (token.lexeme=="delete")
                        token.kind = DELETE;
                    else if (token.lexeme=="int")
                        token.kind = INT;

                    string s1=token.lexeme;
                    string s2;
                    s2=s1.substr(0,1);
                    if (token.kind==NUM && (nextToken.kind==ID || (s1.length()>1 && s2=="0"))){
                        cerr<<"ERROR";
                        return 0;
                    }
                    else if (token.kind==BECOMES && nextToken.kind==BECOMES)
                    {
                         cout <<"EQ =="<<endl;
                         j++;
                    }
                    else if (token.kind==ID && token.lexeme=="NULL")
                    {
                         cout <<"NULL NULL"<<endl;
                         j++;
                    }
                    else
                        cout << kindString(token.kind)<<" "<< token.lexeme<<endl ;
            }
        }


    } catch(string msg) {
        cerr << msg << endl;
    }

    return 0;
}
