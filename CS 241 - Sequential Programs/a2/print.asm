print:

sw $1,-4($30)
sw $2,-8($30)
sw $3,-12($30)
sw $4,-16($30)
sw $5,-20($30)
sw $6,-24($30)
sw $7,-28($30)
sw $8,-32($30)
sw $9,-36($30)
sw $10,-40($30)
sw $11,-44($30)
sw $12,-48($30)
sw $13,-52($30)
sw $14,-56($30)
sw $15,-60($30)
sw $16,-64($30)

lis $3
.word 0xffff000c
lis $2
.word 64
lis $4
.word 10
lis $5
.word 1
lis $6
.word 4
lis $9
.word 45
lis $13
.word 0
lis $14
.word 0
lis $15
.word 48
lis $16
.word 2147483648

sub $30,$30,$2

add $7,$1,$0
slt $8,$7,$0
beq $8,$5,abs

main:
beq $7,$16,spl2
div $7,$4
mflo $10
mfhi $11
sub $30,$30,$6
add $11,$11,$15

sw $11,-64($30)
add $7,$10,$0
add $13,$13,$5
bne $7,$0,main
beq $7,$0,main2

abs:
sw $9,0($3)
sub $7,$0,$7
beq $0,$0,main

main2:
add $14,$14,$5
lw $12,-64($30)
sw $12,0($3)
add $30,$30,$6
bne $14,$13,main2
beq $14,$13,end

spl2:
lis $7
.word 50
sw $7,0($3)

lis $7
.word 49
sw $7,0($3)

lis $7
.word 52
sw $7,0($3)

lis $7
.word 55
sw $7,0($3)

lis $7
.word 52
sw $7,0($3)

lis $7
.word 56
sw $7,0($3)

lis $7
.word 51
sw $7,0($3)

lis $7
.word 54
sw $7,0($3)

lis $7
.word 52
sw $7,0($3)

lis $7
.word 56
sw $7,0($3)
beq $0,$0,end


end:
add $30,$30,$2
sw $4,0($3)

lw $1,-4($30)
lw $2,-8($30)
lw $3,-12($30)
lw $4,-16($30)
lw $5,-20($30)
lw $6,-24($30)
lw $7,-28($30)
lw $8,-32($30)
lw $9,-36($30)
lw $10,-40($30)
lw $11,-44($30)
lw $12,-48($30)
lw $13,-52($30)
lw $14,-56($30)
lw $15,-60($30)
lw $16,-64($30)

jr $31


