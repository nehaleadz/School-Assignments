0000 00 ss sss t tttt dddd d 000 00 10 0000
------------------------------------------------------------------------------------------------------
a2p5:


lis $3
.word 0xffff000c ; output address
lis $4
.word 1
add $5,$1,$0
add $8,$1,$0 
lis $7
.word 65
add $2,$2,$4

loop:
add $8,$8,$7
sw $8,0($3)
add $5,$5,$4
slt $6,$5,$2
bne $6,$0,loop
jr $31
---------------------------------
lis $4
.word 4
lis $3
.word 65
lis $10
.word 1
lis $8
.word -1

add $8,$8,$10  ;$8 to check that loop goes on till the last element
lw $6,0($1)
add $6,$6,$3  ;
sw $7,0($6)   ; $ to display the value

bne $8,$2,-4 

jr $31
------------------------------------------------------------------------------------------------------
a2p4:

lis $4
.word 4
lis $10
.word 1
add $7,$1,$0 ;  $7 is for manipulating addresses
lw $3,0($1)
sub $9,$0,$10

add $9,$9,$10

add $7,$7,$4
lw $6,0($7)
slt $8,$3,$6
beq $8,$0,1
add $3,$6,$0

bne $9,$2,-6


jr $31
------------------------------------------------------------------------------------------------------
a2p3:
beq $2,$0,0 ; if $2=0
lis $3      ; Load immediate and skip in $3; $3=mem[pc];pc=pc+4
.word -1    ; Put a value -1 in the $3
bne $2,$0,0 ; if $2 !=0 then.....
lis $4      ; 
.word 4     ; 
lis $5      ;
.word 1     ;
add $6,$2,$0; Store value of $2 in $6
sub $6,$6,$5;
mult $6,$4  ;
mflo $6     ;
add $6,$6,$1;
lw $3,0($6) ;
jr $31      ;

//comments

lis $4 Load immediate and skip in $4; $4=mem[pc];pc=pc+4
Put a value 4 in the $4
mult $2,$4 hi:lo=$2*$4
mflo $2 Move from low $2=lo
add $2,$1,$2 $2=$1+$2
lw $3,0($2) Load word $3=mem[$2+0]:4
jr $31 pc=$31
beq $2,
----------------comments---------------------------
$4=1 if $1<$2 ;
if ($4==$0) pc+=1*4
$3=$2+$0
if ($3==$2) pc+=1*4
$3=$1+$0
------------------------------------------------------------------------------------------------------
a2p1 and a2p2:

slt $4,$1,$2
beq $4,$0,1
add $3,$2,$0
beq $3,$2,1
add $3,$1,$0
jr $31
----------------comments---------------------------
$4=1 if $1<$2 ;
if ($4==$0) pc+=1*4
$3=$2+$0
if ($3==$2) pc+=1*4
$3=$1+$0
------------------------------------------------------------------------------------------------------

a1p7:

0000 00 00 000 0 0000 0010 0 000 00 01 0100; 0x00002014 Load immediate and skip in $4 (Put a value 1 in $4)
0000 00 00 000 0 0000 0000 0 000 00 00 0001; 0x00000001 Add 1 to $4
0000 00 00 001 0 0100 0001 1 000 00 10 0000; 0x00241820 Add $3,$1,$4 (ie $3 = $1 + $4)
0000 00 11 111 0 0000 0000 0 000 00 00 1000; 0x03e00008; Return
-------------------------------------------------------------------------------------------------------
a1p8:

0000 00 00 000 0 0000 0010 0 000 00 01 0100; 0x00002014 Load immediate and skip in $4 (Put a value 1 in $4)
0000 00 00 000 0 0000 0000 0 000 00 00 0100; 0x00000004 put a value 4 to $4
0000 00 00 001 0 0100 0001 1 000 00 10 0000; 0x0241820; add $1 and $4 put it in $3
0000 00 11 111 0 0000 0000 0 000 00 00 1000; 0x03e00008; Return
------------------------------------------------------------------------------------------------------
new a1p6:
0000 00 00 001 0 0010 0010 0 000 00 10 1010; 0x0022202A; Set less than, compare $1<$2, store it in $4
0001 00 00 100 0 0000 0000 0 000 00 00 0010; 0x10800001; if $4=0,skip next instruction
0000 00 00 010 0 0000 0001 1 000 00 10 0000; 0x00401820; Add $2,$0 and store in $3
0001 00 00 011 0 0010 0000 0 000 00 00 0001; 0x10620001; if $3=$2,skip next instruction
0000 00 00 001 0 0000 0001 1 000 00 10 0000; 0x00201820; Add $1, $0 and store in $3
0000 00 11 111 0 0000 0000 0 000 00 00 1000; 0x03e00008; Return
-----------------------------------------------------------------------------------------------------
a1p5:

0000 00 00 001 0 0000 0001 1 000 00 10 0000;0x00201820 Add $1 $0 and store in $3
0000 00 00 001 0 0011 0010 0 000 00 10 0000;0x00232020 Add  $1 and $3 and out in $4
0000 00 11 111 0 0000 0000 0 000 00 00 1000;0x03e00008 Return
------------------------------------------------------------------------------------------------------
Example 1:

0000 00 00 000 0 0000 0010 1 000 00 01 0100; Load immediate and skip put in reg 5
0000 00 00 000 0 0000 0000 0 000 00 10 1010; Set less than 
0000 00 00 000 0 0000 0011 1 000 00 01 0100; Load immediate and skip put in reg 7
0000 00 00 000 0 0000 0000 0 000 00 11 0100; ??
0000 00 00 101 0 0111 0001 1 000 00 10 0000; Add  $5 and $7 and out in $3
0000 00 11 111 0 0000 0000 0 000 00 00 1000; Jump Register 31
-----------------------------------------------------------------------------------------------------
Example 2:

0000 00 00 001 0 0000 0001 0 000 00 10 1010; function: Set less than, compare $1<0
0001 00 00 010 0 0000 0000 0 000 00 00 0001; if $2==$0 skip next instruction
0000 00 00 000 0 0001 0000 1 000 00 10 0010; $1=0-$1
0000 00 11 111 0 0000 0000 0 000 00 00 1000; return to OS
-----------------------------------------------------------------------------------------------------
0000 00 00 000 0 0000 0001 1 000 00 01 0100; Load immediate and skip put in reg 3
0000 00 00 000 0 0000 0000 0 000 11 00 1001; SHift,Jump And Link Register
0000 00 00 000 0 0000 1001 1 000 00 01 0100; Load immediate and skip put in reg 19
0000 00 00 000 0 0000 0000 0 000 11 00 1000; Shift
0000 00 10 011 0 0011 0000 1 000 00 10 0010; Add values in reg 19 and 3 and put them in reg 1
0000 00 11 111 0 0000 0000 0 000 00 00 1000; Jump reg 31
-----------------------------------------------------------------------------------------------------
0000 00 00 000 0 0000 0000 1 000 00 01 0100; 0x00000814 Load immediate and skip put in reg 1
0000 00 00 000 0 0000 0001 1 000 00 01 0100; 0x00001814 Load immediate and skip put in reg 3
0000 00 00 001 0 0011 0010 0 000 00 10 0000; 0x00232020 Add values in reg 1 and 3 and store in reg 4
0000 00 00 100 0 0000 0000 0 000 00 00 1000; 0x03e00008 Return


0000 00 00 100 0 0000 0000 0 000 00 00 1000; 0x03E00008 Jump reg 4

