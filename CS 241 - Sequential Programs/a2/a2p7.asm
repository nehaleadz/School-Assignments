lis $5
.word 1
lis $10
.word 4
beq $2,$0,end

sw $31,-4($30)
lis $31
.word 4
sub $30,$30,$31
add $4,$0,$0
add $9,$1,$0

loop:
lw $1,0($9)
lis $3
.word print
jalr $3
add $4,$4,$5
add $9,$9,$10
bne $4,$2,loop

lis $31
.word 4
add $30,$30,$31
lw $31,-4($30)

end:
jr $31
