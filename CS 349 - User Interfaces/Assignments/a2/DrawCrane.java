import java.awt.*;
import java.awt.event.*;

import javax.swing.JComponent;

public class DrawCrane extends JComponent implements MouseListener,MouseMotionListener{

	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getOldPivotX() {
		return oldPivotX;
	}
	public int getOldPivotY() {
		return oldPivotY;
	}
	public int getPivotX() {
		return pivotX;
	}
	public int getPivotY() {
		return pivotY;
	}
	private static final long serialVersionUID = 1L;
	private int oldPivotX,oldPivotY,pivotX,pivotY,translationX=0;
	private int frameHeight=700-40,frameWidth=1000;
	private Arm []arms=new Arm[4];
	private Block []blocks=new Block[6];
	private Point lastPt,curMousePosition;
	private boolean insideArms=false;
	int blocksStartPos=frameWidth*1/3;
	int []blockX={blocksStartPos+5,blocksStartPos+115,blocksStartPos,blocksStartPos+85,blocksStartPos+130,blocksStartPos+40};
	int []blockY={frameHeight-125,frameHeight-115,frameHeight-85,frameHeight-85,frameHeight-75,frameHeight-55};
	int []blockWidth={110,40,30,30,30,70};
	int []blockHeight={40,40,100,30,90,70};
	int attachedBlock=-1;
	
	public DrawCrane ()
	{
		super();
		addMouseListener(this);
		addMouseMotionListener(this);
		oldPivotX=175;
		oldPivotY=frameHeight-95;
		curMousePosition=new Point();
		pivotX=180;
		pivotY=frameHeight-95;
		double []initialAngles={-50,-20,10,0};
		int []initialX={pivotX-15,pivotX-45+80,pivotX-45+80*2,pivotX-45+80*3};
		int []initialY={pivotY-5,pivotY-70,pivotY-100,pivotY-85};
		for (int i=0;i<arms.length;i++){
			arms[i]=new Arm(initialX[i],initialY[i]);
			arms[i].setInitialRotationAngle(initialAngles[i]);
		}
		for (int i=0;i<blocks.length;i++){
			blocks[i]=new Block(blockX[i],blockY[i],blockWidth[i],blockHeight[i]);
		}
		
		
	}

	public void actionPerformed(ActionEvent ev)
    {}
    public void mousePressed(MouseEvent e)
    {
    	lastPt = e.getPoint();
    	//System.out.println("Mouse Pressed");  
    }
    public void mouseReleased(MouseEvent e)
    {
    	//System.out.println("Mouse Released");
    }
    public void mouseEntered(MouseEvent e){
    	//System.out.println("Mouse Entered");
    }
    public void mouseExited(MouseEvent e){
    	//System.out.println("Mouse Exited");
    }
    public void mouseMoved(MouseEvent e)
    {
    	//System.out.println("Mouse Moved");
    }
    public void mouseClicked(MouseEvent e)
    {
    	Point pClicked=new Point();
    	pClicked.x=e.getX();
    	pClicked.y=e.getY();
    	if (arms[3].electromagnetContainsPoint(pClicked)){
    		
    		if (attachedBlock==-1)
    			attachBlock(pClicked);
    		else
    			detachBlock();
    	}
    }
    public void detachBlock(){
    	System.out.println("Detach block");
    	blocks[attachedBlock].x=blockX[attachedBlock];
    	blocks[attachedBlock].y=blockY[attachedBlock];
    	attachedBlock=-1;
    	arms[3].setBlock(null);
    	repaint();
    }
    public void attachBlock(Point p){
    	for (int i=0;i<blocks.length;i++){
    		if (p.x>=blocks[i].x && (p.x<=blocks[i].x+blocks[i].width) && p.y<(blocks[i].y)){
    			System.out.println("Block is "+i);
    			blocks[i].x=p.x;
    			blocks[i].y=p.y;
    			arms[3].setBlock(blocks[i]);
    			
    			//blocks[i].x=blocks[i].y=blocks[i].width=blocks[i].height=0;
    			attachedBlock=i;
    			repaint();
    			break;
    		}
    	}
    }
    public void mouseDragged(MouseEvent e)
    {
        curMousePosition.x=e.getX();
        curMousePosition.y=e.getY();
    	
        for (int i=0;i<4;i++){
        	if (arms[i].containsPoint(curMousePosition, lastPt)==1){
        		int j=i;
        		insideArms=true;
        		if (i==0){
        			arms[i].setBaseX(oldPivotX);
        			arms[i].setBaseY(oldPivotY);
        		}
    			j++;
    			while (j<4){
        			arms[j].setBaseX(arms[j-1].getNewX());
        			arms[j].setBaseY(arms[j-1].getNewY());
        			arms[j].updateNewCoordinates(curMousePosition, lastPt);
        			j++;
        		}      		
        		break;
        	}
        	else
        		insideArms=false;
        }
        if (!insideArms && e.getX()>=0 && e.getX()<frameWidth){
        	translationX = e.getX()-oldPivotX;
        }
        lastPt = e.getPoint();
        repaint();   
    }
    public void paintComponent(Graphics g){
		Graphics2D g2=(Graphics2D)g;
		Color blueColor = new Color(62,107,182);
		Color grayColor = new Color(71,71,71);
		Color greenColor = new Color(28,167,45);
		
		Stroke dashedStroke = new BasicStroke(4, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 2.5f, new float[]{10}, 0);
		
		int[] xBody ={100+translationX,100+translationX,130+translationX,220+translationX,250+translationX,250+translationX};
		int[] yBody ={frameHeight,frameHeight-50 ,frameHeight-80 ,frameHeight-80 ,frameHeight-50 ,frameHeight };
	     
		int[] xTriangle ={160+translationX,175+translationX,190+translationX};
		int[] yTriangle ={frameHeight-80 ,oldPivotY ,frameHeight-80 };	
		
		
		g2.setColor(blueColor);
		
		Rectangle baseR=new Rectangle();
		baseR.x=80+translationX;
		baseR.y=frameHeight-5;
		baseR.width=190;
		baseR.height=20;
		
		for (int i=0;i<blocks.length;i++){
			if (i!=attachedBlock)
				blocks[i].paintComponent(g);
		}
		g2.setColor(blueColor);
		
		Polygon body= new Polygon();		
		for (int i=0;i<xBody.length;i++)
			body.addPoint(xBody[i],yBody[i]);
		
		if (curMousePosition!=null){
			if (!body.contains(curMousePosition) && !baseR.contains(curMousePosition) &&!insideArms){
				for (int i=0;i<xBody.length;i++){
					xBody[i]=xBody[i]-translationX;
					body.reset();
					body.addPoint(xBody[i],yBody[i]);
				}
				for (int i=0;i<xTriangle.length;i++)
					xTriangle[i]=xTriangle[i]-translationX;
				baseR.x=80;				
				translationX=0;
			}
			
		}
//		
		g2.fillPolygon(xBody, yBody, xBody.length);	
			
		Polygon triangle= new Polygon();
		for (int i=0;i<xTriangle.length;i++)
			triangle.addPoint(xTriangle[i],yTriangle[i]);
		g2.fill(triangle);
			    		
		g2.setStroke(new BasicStroke(1));//outline of an object
	    g2.setColor(Color.BLACK);
    
	    g2.draw(triangle); //if we don't add it we won't see strokes
	    g2.draw(body);
	    
	    /* Grass */
	    g2.setPaint(greenColor); 
		g2.setStroke(new BasicStroke(5));//outline of an object
		g.drawLine(0, frameHeight+15,frameWidth , frameHeight+15);
		
	    /* Base of crane */   
		g2.setColor(grayColor);
		g2.fill(baseR);
		g2.setStroke(dashedStroke);//outline of an object
		g2.setPaint(Color.BLACK); 
		g2.draw(baseR);
		
		for (int i=0;i<arms.length;i++){
        	arms[i].setTranslation(translationX);
        	arms[3].setElectromagnetToVisible(true);
        	arms[i].paintComponent(g);
		}
	}
	
}