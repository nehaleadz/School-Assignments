import javax.swing.*;

public class CraneSimulator extends JComponent{

	private static final long serialVersionUID = 1L;
		
	public static void main(String[] args) {
		
		JFrame frame= new JFrame("Crane Simulator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1000,700);//(1440,700)
		
		DrawCrane crane=new DrawCrane();
		frame.setContentPane(crane);
		
		frame.setVisible(true);	    
	}
}