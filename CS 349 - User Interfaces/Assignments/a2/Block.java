
import java.awt.*;

import javax.swing.JComponent;

public class Block extends JComponent{

	private static final long serialVersionUID = 1L;
	int x,y,width,height;
	public Block(int bx,int by,int bWidth, int bHeight){
		super();
		x=bx;
		y=by;
		width=bWidth;
		height=bHeight;
	}
	public void paintComponent(Graphics g){
		
		Graphics2D g2=(Graphics2D)g;
		Color blueColor = new Color(62,107,182);
		
		int frameHeight=700-40;
		int frameWidth=1000/2;
			    
	    g2.setColor(blueColor);
	    g.fillRect(x,y,width,height);
		//g.fillRect(frameWidth,frameHeight-60, 30, 72);
		
		g2.setStroke(new BasicStroke(1));//outline of an object
	    g2.setPaint(Color.BLACK); 
	    g.drawRect(x,y,width,height);

	}
}