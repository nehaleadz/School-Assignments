import java.awt.*;
import java.awt.geom.*;

import javax.swing.JComponent;

public class Arm extends JComponent{
	private static final long serialVersionUID = 1L;
	private double baseX,baseY,translationX,newX=0,newY=0;
	double oldAngle;
	double []angles;
	private Rectangle r;
	private boolean isVisible;
	private Rectangle electromagnetR;
	private Block b;
	
	public Arm(int pivotX,int pivotY){
		super();
		electromagnetR=new Rectangle();
		
		baseX=pivotX;
		baseY=pivotY;
		translationX=0;
		newX=baseX+90;
		newY=baseY;
		r=new Rectangle(0,0, 90, 20);
	}
	public void setInitialRotationAngle(double rotAngle){
		oldAngle=rotAngle;
	}
	public double getAngle(){
		return oldAngle;
	}
	public void setBaseX(double x){
		baseX=x;
	}
	public void setBaseY(double y){
		baseY=y;
	}
	public double getBaseX(){
		return baseX;
	}
	public double getNewY(){
		return newY;
	}
	public double getNewX(){
		return newX;
	}
	public double getBaseY(){
		return baseY;
	}
	public void setTranslation(int x){
		translationX=x;
	}
	public Point updateNewCoordinates(Point p, Point lastPt){
		double dx1 = baseX-p.x;
		double dy1 = baseY-p.y;
		float delta1 = (float)Math.atan2(dx1, dy1);
		
		double dx2 = baseX-lastPt.x;
		double dy2 = baseY-lastPt.y;
		float delta2 = (float)Math.atan2(dx2,dy2);
		
		oldAngle+= Math.toDegrees(delta2-delta1);
			
		newX = 80*Math.cos(Math.toRadians(oldAngle))- 10*Math.sin(Math.toRadians(oldAngle));
	    newY =80*Math.sin(Math.toRadians(oldAngle))+ 10*Math.cos(Math.toRadians(oldAngle));
		
	    newX +=baseX+translationX;
		newY += baseY-5;
		
		//System.out.println("ANGLE "+oldAngle);
		Point newP=new Point();
		newP.x=(int) newX;
		newP.x=(int) newY;
		
		return newP;
	}
	public boolean electromagnetContainsPoint(Point p){
		//Transforming the mouse co-ordinates and 
		//applying the same transformations that have been applied to the arms
		
		AffineTransform trans =  new AffineTransform();
		trans.translate(baseX+translationX, baseY);
		trans.rotate(Math.toRadians(oldAngle),r.x, r.y);
		Point2D tp=null;

		try {
			if (p!=null)
				tp = trans.inverseTransform(p, tp);
		} catch (NoninvertibleTransformException e) {
			e.printStackTrace();
		}
		if (p!=null &&electromagnetR.contains(tp))
			return true;
		else
			return false;
	}
	public int containsPoint(Point p, Point lastPt){
		
		//Transforming the mouse co-ordinates and 
		//applying the same transformations that have been applied to the arms
		
		AffineTransform trans =  new AffineTransform();
		trans.translate(baseX+translationX, baseY);
		trans.rotate(Math.toRadians(oldAngle),r.x, r.y);
		Point2D tp=null;
		try {
			if (p!=null)
				tp = trans.inverseTransform(p, tp);
		} catch (NoninvertibleTransformException e) {
			e.printStackTrace();
		}
			
		if (p!=null && r.contains(tp)){
			updateNewCoordinates(p,lastPt);
			return 1;
		}
		return -1;
	}
	public void setBlock(Block blockAttached){
		b=blockAttached;
	}
	public void drawMagnet(Graphics g){
		Graphics2D g2=(Graphics2D)g;
		
		AffineTransform trans =  new AffineTransform();	
		trans.translate(baseX+translationX, baseY);
		trans.rotate(Math.toRadians(oldAngle));
		g2.setTransform(trans);
		
	    electromagnetR.x=90;
	    electromagnetR.y=-15;
	    electromagnetR.height=50;
	    electromagnetR.width=10;

	    g2.fill(electromagnetR);
		    
	    if (b!=null){
	    	g.fillRect(100,-10,b.height,b.width);
			
			g2.setStroke(new BasicStroke(1));//outline of an object
		    g2.setPaint(Color.BLACK); 
		    g.drawRect(100,-10,b.height,b.width);
	    }
	    g2.setStroke(new BasicStroke(1));//outline of an object
	   
	    g2.setPaint(Color.BLACK); 
	    g2.draw(electromagnetR);
	    
	    trans.rotate(-Math.toRadians(oldAngle));
		trans.translate(-(baseX+translationX), -baseY);
		
		g2.setTransform(trans);
	}
	public void drawAttachedBlock(Graphics g,Block b){
		Graphics2D g2=(Graphics2D)g;
		
		AffineTransform trans =  new AffineTransform();	
		trans.translate(baseX+translationX, baseY);
		trans.rotate(Math.toRadians(oldAngle));
		g2.setTransform(trans);
		
		b.x=90;
		b.y=-15;
		
		b.paintComponent(g);
		
	    trans.rotate(-Math.toRadians(oldAngle));
		trans.translate(-(baseX+translationX), -baseY);
		
		g2.setTransform(trans);
	}
	public void setElectromagnetToVisible(boolean isMagnetVisible){
		isVisible=isMagnetVisible;
	}
	public void paintComponent(Graphics g){
		 
		Graphics2D g2=(Graphics2D)g;
		Color blueColor = new Color(62,107,182);
		g2.setColor(blueColor);
		
		AffineTransform trans =  new AffineTransform();
		
		trans.translate(baseX+translationX, baseY);
		trans.rotate(Math.toRadians(oldAngle));
		g2.setTransform(trans);
		
		g2.fill(r);
		g2.setColor(Color.RED);
		g2.fillOval(10,5 , 7, 7); 
					
		g2.setStroke(new BasicStroke(1));
	    g2.setPaint(Color.BLACK); 
	    	
	    g.drawRect(0, 0, 90, 20); 		
		g2.setColor(blueColor);
		
		if (isVisible){
			drawMagnet(g);
		}
		trans.rotate(-Math.toRadians(oldAngle));
		trans.translate(-(baseX+translationX), -baseY);
		
		g2.setTransform(trans);
		
		
	    
	}
}
