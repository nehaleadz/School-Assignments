****************** CRANE SIMULATOR ******************
To run the project, type "make run" on terminal

Voila!! Have fun working with the crane :)

How to use:

1. Click on an arm and then drag it to rotate it
2. Click on the crane and drag it to translate it
3. Bring an electromagnet near the blocks and click on the electromagnet to attach the blocks to it. To release the block click on the electromagnet again.

Enhancements:

1. The crane will always remain in the screen. User cannot drag it outside the screen
2. Used different sized blocks which the user can use to pick up with the electromagnet.

