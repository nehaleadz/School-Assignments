import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class ColorPalette extends JPanel implements ActionListener{
	
	private DoodleModel model;
	private JButton[] colorsButton = new JButton[20];
	private Color currentColor=new Color(0,0,0);
	
	public ColorPalette (DoodleModel dModel){
		super();
		this.model=dModel;
		this.layoutView();
		this.model.addView(new IView() {
			public void updateView() {	
				//System.out.println("Color Palette resized");
			}
		});
		
	}
	
	public void colorInit(){
		Color greenColor = new Color(28,167,45);
		Color blueColor = new Color(0,0,153);
		Color pinkColor = new Color(255,153,255);
		Color grayColor = new Color(96,96,96);
		Color purpleColor = new Color(76,0,153);
		Color maroonColor=new Color(102,0,0);
		
		for(int i=0; i<colorsButton.length; i++){
			colorsButton[i] = new JButton("  ");
			colorsButton[i].addActionListener(this);
			colorsButton[i].setBorderPainted(false);
			colorsButton[i].setOpaque(true);
		}
		colorsButton[0].setBackground(Color.white);
		colorsButton[1].setBackground(Color.black);
		colorsButton[2].setBackground(Color.gray);
		colorsButton[3].setBackground(Color.darkGray);
		colorsButton[4].setBackground(Color.red);
		colorsButton[5].setBackground(maroonColor);
		colorsButton[6].setBackground(Color.yellow);
		colorsButton[7].setBackground(Color.green);	
		colorsButton[8].setBackground(Color.cyan);
		colorsButton[9].setBackground(Color.blue);	
		colorsButton[10].setBackground(Color.pink);
		colorsButton[11].setBackground(purpleColor); 
		colorsButton[12].setBackground(Color.orange);
		colorsButton[13].setBackground(Color.magenta);
		colorsButton[14].setBackground(blueColor);
		colorsButton[15].setBackground(pinkColor);
		colorsButton[16].setBackground(Color.lightGray);
		colorsButton[17].setBackground(grayColor);
		colorsButton[18].setBackground(purpleColor); 	
		colorsButton[19].setBackground(greenColor);			
	}
	
	private void layoutView(){
		
		JToolBar toolbar = new JToolBar();	
		toolbar.setLayout(new GridLayout(10,2));
		colorInit();
        for (int i=0;i<20;i++){
        	colorsButton[i].setPreferredSize(new Dimension(30, 30));
        	toolbar.add(colorsButton[i]);
        }
        toolbar.setFloatable(false);
        add(toolbar, BorderLayout.EAST);
	}

	public void actionPerformed(ActionEvent e) {
		for (int i = 0; i < colorsButton.length; i++) {
			if (e.getSource() == colorsButton[i]) {
					currentColor=colorsButton[i].getBackground();
					model.setCurrentColor(currentColor);
					model.colorsUsed.add(currentColor);
					model.setDraw(true);
			}
		}
	}
}
