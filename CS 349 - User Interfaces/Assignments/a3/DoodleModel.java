import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

public class DoodleModel  extends Object{

	private ArrayList<IView> views = new ArrayList<IView>();
	private boolean bDraw,bErase,bSelect;
	ArrayList <Color> colorsUsed=new ArrayList<Color>();
	private Color currentColor;
	private ArrayList<ArrayList<Point>>drawImage=new ArrayList<ArrayList<Point>>();
	private ArrayList<Point> selectionImagePoints = new ArrayList<Point>();
	private ArrayList<Point>drawPoints = new ArrayList<Point>();	
	private ArrayList<ArrayList<Point>> selectedImages = new ArrayList<ArrayList<Point>>();//The images that have been selected
	private ArrayList <Point> displacement =new ArrayList<Point>();
	private ArrayList <Integer>stopTimeForImages=new ArrayList<Integer>();
	
	private ImageIcon draw = new ImageIcon("./Icons/Brush.png","Brush");
	private ImageIcon erase = new ImageIcon("./Icons/Eraser.png","Erase");
	private ImageIcon select = new ImageIcon("./Icons/Cut.png","Cut");
	private ImageIcon play = new ImageIcon("./Icons/Play.png","Play");
	private ImageIcon pause = new ImageIcon("./Icons/Pause.png","Pause");
	private ImageIcon replay = new ImageIcon("./Icons/replay.jpg","Replay");
	private ImageIcon newFile = new ImageIcon("./Icons/New.png","New");
	private ImageIcon insertFrames = new ImageIcon("./Icons/Frames.png","Frames");
	private ImageIcon open = new ImageIcon("./Icons/Open.png","Open");
	private ImageIcon save = new ImageIcon("./Icons/Save.png","Save");


	
	private int fps, sliderValue,maxSliderValue,stopTime,translationX, translationY,curTime;
	private boolean runAnimation, startDragSelectedObj,bStartSelection;
	boolean drawCircle;
	boolean drawEllipse;
	private Timer timer;
	private Point start;
	private JSlider slider;
	int x1,x2,y1,y2;
	
	public DoodleModel() {
		super();
		bDraw=true;
		bErase=bSelect=bStartSelection=runAnimation=startDragSelectedObj=false;
		sliderValue=translationX=translationY=curTime=0;
		currentColor=Color.black;
		fps=10;
		
		slider=new JSlider();
		ActionListener repainter = new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				if (runAnimation==false){
					Point d=new Point(translationX,translationY);
	        		displacement.add(d);
	        		sliderValue++;
	        		stopTime++;
	        		slider.setValue(sliderValue);
					
					if (sliderValue>maxSliderValue)
						maxSliderValue=sliderValue+1;
					slider.setMaximum(maxSliderValue);
				}
				else{
					//System.out.println("Running animation in model");
					curTime++;
					slider.setValue(curTime);
					updateAllViews();
					if (curTime>=stopTime){
						timer.stop();
						runAnimation=false;
					}
					
				}
				
        	}
		};
		timer = new Timer(100/fps, repainter);
	}
	
	/** Add a new view of this triangle. */
	public void addView(IView view) {
		this.views.add(view);
		view.updateView();
	}

	/** Remove a view from this triangle. */
	public void removeView(IView view) {
		this.views.remove(view);
	}

	/** Update all the views that are viewing this triangle. */
	private void updateAllViews() {
	
		for (IView view : this.views) {
			view.updateView();
		}
	}
	
	public void setDraw(boolean value) {
		bDraw=value;
		bErase=false;
		bSelect=false;
		this.updateAllViews();
	}
	public void setErase(boolean value) {
		bErase=value;
		bDraw=false;
		bSelect=false;
		this.updateAllViews();
	}
	public void setSelect(boolean value) {
		bSelect=value;
		bDraw=false;
		bErase=false;
		this.updateAllViews();
	}
	public void setbStartSelection(boolean value) {
		bStartSelection=value;
	}
	public void setCurrentColor(Color c){
		currentColor=c;
	}
	public void setStartDragSelectedObj(boolean value){
		startDragSelectedObj=value;
	}
	public void setStopTime(int i){
		stopTime=i;
	}
	public void setDrawImage(ArrayList<ArrayList<Point>> array){
		drawImage=array;
	}
	public void setSelectionImagePoints(ArrayList<Point> array){
		selectionImagePoints=array;
	}
	public void setDrawPoints(ArrayList<Point> array){
		drawPoints=array;
	}
	public void setSelectedImages(ArrayList<ArrayList<Point>> array){
		selectedImages=array;
	}
	public void setDisplacement(ArrayList<Point> array){
		displacement=array;
	}
	public void setRunAnimation(boolean value){
		runAnimation=value;
	}
	public void setTranslationX(int val){
		translationX=val;
	}
	public void setTranslationY(int val){
		translationY=val;
	}
	public void setCurTime(int val){
		curTime=val;
	}
	public void setStart(Point p){
		start=p;
	}
	public void setSliderValue(int val){
		sliderValue=val;
	}
	public void setMaxSliderValue(int val){
		maxSliderValue=val;
	}
	public void setStopTimeForImages(ArrayList<Integer> stopTimeArray){
		stopTimeForImages=stopTimeArray;
	}
	/****** Getters *********/
	
	public boolean getDraw() {
		return bDraw;
	}
	public boolean getErase() {
		return bErase;
	}
	public boolean getSelect() {
		return bSelect;
	}
	public boolean getbStartSelection() {
		return bStartSelection;
	}
	public ImageIcon getDrawIcon() {
		return draw;
	}
	public ImageIcon getEraseIcon() {
		return erase;
	}
	public ImageIcon getSelectIcon() {
		return select;
	}
	public ImageIcon getPlayIcon() {
		return play;
	}
	public ImageIcon getPauseIcon() {
		return pause;
	}
	public ImageIcon getReplayIcon() {
		return replay;
	}
	public ImageIcon getInsertFramesIcon() {
		return insertFrames;
	}
	public ImageIcon getNewFileIcon() {
		return newFile;
	}
	public ImageIcon getSaveIcon() {
		return save;
	}
	public ImageIcon getOpenIcon() {
		return open;
	}
	public Color getCurrentColor(){
		return currentColor;
	}
	public int getFps(){
		return fps;
	}
	public int getSliderValue(){
		return sliderValue;
	}
	public JSlider getSlider(){
		return slider;
	}
	public boolean getRunAnimation(){
		return runAnimation;
	}
	public int getStopTime(){
		return stopTime;
	}
	public Timer getTimer(){
		return timer;
	}
	public ArrayList<ArrayList<Point>> getDrawImage(){
		return drawImage;
	}
	public ArrayList<Point> getSelectionImagePoints(){
		return selectionImagePoints;
	}
	public ArrayList<Point> getDrawPoints(){
		return drawPoints;
	}
	public ArrayList<ArrayList<Point>> getSelectedImages(){
		return selectedImages;
	}
	public ArrayList<Point> getDisplacement(){
		return displacement;
	}
	public boolean getStartDragSelectedObj(){
		return startDragSelectedObj;
	}
	public int getTranslationX(){
		return translationX;
	}
	public int getTranslationY(){
		return translationY;
	}
	public int getCurTime(){
		return curTime;
	}
	public Point getStart(){
		return start;
	}
	public int getMaxSliderValue(){
		return maxSliderValue;
	}
	public ArrayList<Integer>getStopTimeForImages(){
		return stopTimeForImages;
	}
}
