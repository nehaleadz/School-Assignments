import java.awt.*;
import javax.swing.*;

public class Main {

	private static JFrame frame= new JFrame("K-Sketch");

	public static void main(String[] args) {
			
		DoodleModel model = new DoodleModel();
		ColorPalette colorPalette= new ColorPalette(model);
		DrawPad drawPad=new DrawPad(model);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		frame.setMinimumSize(new Dimension(screenSize.width-250, screenSize.height-300));
		frame.add(drawPad, BorderLayout.CENTER);
		frame.add(colorPalette, BorderLayout.EAST);
		frame.setVisible(true);	 
		
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		
	}
	
}
