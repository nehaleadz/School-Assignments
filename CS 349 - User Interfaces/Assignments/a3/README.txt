

Functionality Implemented
	
	- Clicking on inserting frames we can insert 10 frames at a time. The frame with current transformations is added 10*number of times user clicks on button.
	  Eg: If slider is at time 100ms and then clicks on the button 2 times then from 100-120 the animation would have the same transformation, i.e it will be still.
	
	- Sliding works only in pause mode. When we reach the end and try to slide it won't work. We can always click on replay button to restart the animation.

	- To start everything afresh, one can click on New button, which clears everything (including animation)

	- One can also hover on the buttons to see their functionality
 
Enhancements

	- One can save a drawing and open it. The file is saved in dat format. When saving a file you just have to type the file name.Open previously created files Eg: mickey.dat and donaldDuck.dat
	- Tried to implement color Palette. To select a color, one needs to double click on the colour button.
