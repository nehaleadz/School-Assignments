import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

class DrawPad extends JPanel{
	
	private DoodleModel model;
	
	private static final long serialVersionUID = 1L;
	private Polygon selectionRegion=new Polygon();
	private Cursor eraserCursor,drawCursor,selectCursor;
	private JButton playPauseB,drawB,eraseB,selectB,replayB,pauseB,insertFramesB,newB,saveB,openB;
	private JToolBar toolbar,animationToolbar, drawingToolbar;
	
	public DrawPad(DoodleModel dModel) {
		super();
		this.model=dModel;
		this.layoutView();
		this.registerControllers();
		this.model.addView(new IView() {
			public void updateView() {
				
				if (model.getDraw()){
					repaint();
				}
				else if (model.getSelect()){
					repaint();
				}
				else if (model.getErase()){
					clear(new Point(0,0));
				}
			}

		});
	}
	
	private void layoutView(){
		
		toolbar = new JToolBar();	
		animationToolbar = new JToolBar();		
		drawingToolbar=new JToolBar();
		
		eraserCursor = getToolkit().createCustomCursor(model.getEraseIcon().getImage(), new Point(0,0), "eraseCursor");
		drawCursor = getToolkit().createCustomCursor(model.getDrawIcon().getImage(), new Point(0,25), "drawCursor");
		selectCursor=getToolkit().createCustomCursor(model.getSelectIcon().getImage(), new Point(0,0), "selectCursor");
		setCursor(drawCursor);
		
		newB = new JButton(model.getNewFileIcon());
		drawB = new JButton(model.getDrawIcon());
		eraseB = new JButton(model.getEraseIcon());
		selectB = new JButton(model.getSelectIcon());
		drawB.setSelected(true);
		
		saveB = new JButton(model.getSaveIcon());
		openB = new JButton(model.getOpenIcon());
				
		newB.setToolTipText("New");
        drawB.setToolTipText("Draw");
        eraseB.setToolTipText("Erase");
        selectB.setToolTipText("Select");
        
        toolbar.add(newB);
        toolbar.add(drawB);
        toolbar.add(eraseB);
        toolbar.add(selectB);
        toolbar.add(saveB);
        toolbar.add(openB);
        toolbar.setFloatable(false);
        toolbar.setAlignmentX(0);
        		
		model.getSlider().setPaintLabels(true);
		//model.getSlider().setPaintTicks(true);
	    model.getSlider().setMajorTickSpacing(100);
		model.getSlider().setMinorTickSpacing(50);
        model.getSlider().setPaintTicks(true);
        model.getSlider().setPreferredSize(new Dimension(700,10));
        model.getSlider().setValue(0);
        
        playPauseB = new JButton(model.getPlayIcon());
        playPauseB.setToolTipText("Play");
        //playPauseB.setEnabled(false);
        pauseB = new JButton(model.getPauseIcon());
        pauseB.setToolTipText("Pause");
        replayB = new JButton(model.getReplayIcon());
        replayB.setToolTipText("Replay");
        insertFramesB = new JButton(model.getInsertFramesIcon());
        insertFramesB.setToolTipText("Insert Frames");
        //replayB.setEnabled(false);
        
        animationToolbar.add(playPauseB);
        animationToolbar.add(pauseB);
        animationToolbar.add(replayB);
        animationToolbar.add(insertFramesB);
        animationToolbar.add(model.getSlider());
        animationToolbar.setFloatable(false);
        
        add(toolbar, BorderLayout.NORTH);
        
        add(animationToolbar,BorderLayout.NORTH);
        
	}
	
	private void registerControllers() {
			
		playPauseB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.setSelectionImagePoints(new ArrayList<Point>());
				model.getTimer().start();
				model.setRunAnimation(true);
				repaint();
			}
		});
		
		pauseB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.getTimer().stop();
				repaint();
			}
		});
		
		replayB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//System.out.println("Replaying.....");
				model.setSelectionImagePoints(new ArrayList<Point>());
				model.getSlider().setValue(0);
				model.setCurTime(0);
				model.getTimer().restart();
				model.setRunAnimation(true);
				repaint();
			}
		});
		
		insertFramesB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.setStopTime(model.getStopTime()+10);
				model.setMaxSliderValue(model.getMaxSliderValue()+10);
				model.getSlider().setMaximum(model.getMaxSliderValue());
				
				//Inserting the frame with same transformations 
				for (int i=0;i<10;i++)
        			model.getDisplacement().add(model.getCurTime(),model.getDisplacement().get(model.getCurTime()));
        	}
		});
		
		model.getSlider().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				
				
				if (model.getRunAnimation() && model.getSlider().getValue()<model.getSliderValue()){ 	//By clicking and dragging the slider, you can move backward and forward through the animation in real-time.
					model.setCurTime(model.getSlider().getValue());
					repaint();
				}
				//System.out.println(model.getMaxSliderValue()+" "+model.getRunAnimation()+" "+model.getCurTime()+" "+model.getSlider().getValue()+" "+model.getStopTime());
			}
	    });
		
		newB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startNew();
			}
		});
		
		drawB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//System.out.println("draw");
				setCursor(drawCursor);
				model.setDraw(true);
				drawB.setSelected(true);
				eraseB.setSelected(false);
				selectB.setSelected(false);
			}
		});
		
		eraseB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.setErase(true);
				setCursor(eraserCursor);
				drawB.setSelected(false);
				eraseB.setSelected(true);
				selectB.setSelected(false);
				repaint();
			}
		});
		
		selectB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//System.out.println("select");
				model.setSelect(true);
				setCursor(selectCursor);
				eraseB.setSelected(false);
				drawB.setSelected(false);
				selectB.setSelected(true);
			}
		});
		
		saveB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		
		openB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				open();
			}
		});
		
		toolbar.addMouseListener(new MouseAdapter() {
			
			public void mouseEntered(MouseEvent e) {
					setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		});
		
		addMouseListener(new MouseAdapter() {
				
				public void mousePressed(MouseEvent e) {
					if (model.getDraw()){
						setCursor(drawCursor); 
					}
					else if (model.getSelect()){
						model.setStart(e.getPoint());
						setCursor(selectCursor); 
					}
					else if (model.getErase()){
						setCursor(eraserCursor); 
					}
				}
				
				public void mouseReleased(MouseEvent ev)  {
					
					if (model.getSelect()){
						//System.out.println("size of selectionRegion Before "+model.getSelectionImagePoints().size());
						for (int i=0;i<model.getSelectionImagePoints().size();i++)
							selectionRegion.addPoint(model.getSelectionImagePoints().get(i).x, model.getSelectionImagePoints().get(i).y);
						
						for (int i=0;i<model.getDrawImage().size();i++){
							int pointsContained=0;
							ArrayList<Point> drawP = model.getDrawImage().get(i);
							for (int j=0;j<drawP.size();j++){
								if (selectionRegion.contains(drawP.get(j))){
									pointsContained++;
								}
							}
							if (pointsContained==drawP.size()){
								model.getSelectedImages().add(drawP);
							}
						}
						model.setbStartSelection(true);
						
						if (model.getSelectedImages().size()==0){
							model.setStartDragSelectedObj(false);
							model.setbStartSelection(false);
			        		model.setSelectionImagePoints(new ArrayList<Point>());
			        	}
						//System.out.println(selectedImages.size()+" Images selected");
						setCursor (Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						model.getTimer().stop();
						model.setStopTime(model.getSliderValue());
						for (int i=0;i<model.getStopTimeForImages().size();i++){
							model.getStopTimeForImages().set(i, model.getSliderValue());
						}
						playPauseB.setEnabled(true); //enable play button as soon as the selection is made
						//System.out.println("Stop Time "+model.getStopTime());
					}
					else if (model.getDraw()){
						model.getDrawImage().add(model.getDrawPoints());
						model.getStopTimeForImages().add(model.getStopTime());
						model.setDrawPoints(new ArrayList<Point>());
					}
					else if (model.getErase()){
						
					}
					repaint();
				}
			
				public void mouseExited(MouseEvent e) {
					 setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					 repaint();
				 }
		});
		
		addMouseMotionListener(new MouseMotionAdapter() {
			 int size = 1000000000;
		 
			public void mouseDragged(MouseEvent e) {
				
				if (model.getDraw()){
					setCursor(drawCursor);
					Point p = e.getPoint();
					model.getDrawPoints().add(p);
				}
				else if (model.getSelect()){
					Point p = e.getPoint();
					//System.out.println(model.getStartDragSelectedObj()+" "+model.getbStartSelection()+ ""+model.getSelectionImagePoints().size());
					if (model.getStartDragSelectedObj()==true){
					 		setCursor (Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					 		//System.out.println(model.getSliderValue() +" "+(p.x-model.getStart().x)+" "+p.x+" "+model.getStart().x);
					 		model.setTranslationX(p.x-model.getStart().x);
					 		model.setTranslationY(p.y-model.getStart().y);
					}
					if (model.getbStartSelection()==true){
				 		setCursor(selectCursor); 
			           	model.setStartDragSelectedObj(true);
			           	model.getTimer().start();
			        }
		            else if(model.getSelectionImagePoints().size() < size) {
		            	setCursor(selectCursor); 
		            	model.getSelectionImagePoints().add(p);
			        }
				}
				else if (model.getErase()){
					setCursor(eraserCursor);
					clear(e.getPoint());
				}
				repaint();
			}
			
		});
	}
	
	private void clear(Point p) {
		//System.out.println("Clear");
		setCursor(eraserCursor); 
		Rectangle r=new Rectangle();
		r.setFrameFromCenter(p.x, p.y, p.x+5, p.y+5);
		
		boolean contains=false;
		
		for (int i=0;i<model.getDrawImage().size();i++){
			ArrayList<Point> drawP = model.getDrawImage().get(i);
			for (int j=0;j<drawP.size();j++){
				if (r.contains(drawP.get(j))){
					
					if (model.getRunAnimation()){ // if it is running
						model.getStopTimeForImages().set(i, model.getCurTime());
					}
					else{
						model.getDrawImage().remove(i);
					}
					
					contains=true;
					break;
				}
			}
			if (contains)
				break;
		}
		model.setSelectionImagePoints(new ArrayList<Point>());
		model.setStartDragSelectedObj(false);
		model.setSelectedImages(new ArrayList<ArrayList<Point>>());
		model.setbStartSelection(false);
		repaint();
	}
	
	private void drawObject(Graphics g,ArrayList<Point>drawP){ //Draw an object which is a collection of points
		Graphics2D g2 = (Graphics2D)g;

		for(int j = 0; j <drawP.size()-1; j++) {
            Point p = drawP.get(j);
            Point next = drawP.get(j+1);
            if (p.distance(next)<=100)
            	g2.draw(new Line2D.Double(p, next));
    	}
	}
	
	private void drawImage(Graphics g){	 //Draw all the objects which are drawn on the screen which are  themselves a collection of points
		Color curColor = Color.black;
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		
		for (int i=0;i<model.getDrawImage().size();i++){	
			if (i<model.colorsUsed.size()-1)
        		curColor=model.colorsUsed.get(i);
			g2.setColor(curColor);
        	ArrayList<Point> drawP = model.getDrawImage().get(i);
        	if (!model.getRunAnimation())// && model.getStopTimeForImages().get(i)<=model.getCurTime())
            		drawObject(g,drawP);
		}
	}

	private void startNew() {
		
		//Re-start the state of the application
		//Clear all the previous data an reinitialize all the variables
		model.getDrawImage().clear();
		model.getDrawPoints().clear();
		model.colorsUsed.clear();
		model.setDraw(true);
		
		model.getSelectedImages().clear();
		model.getSelectionImagePoints().clear();
		model.setSelectionImagePoints(new ArrayList<Point>());
		model.setSelectedImages(new ArrayList<ArrayList<Point>>());
		
		model.setStartDragSelectedObj(false);
		model.setbStartSelection(false);
		
		model.getTimer().stop();
		model.getStopTimeForImages().clear();
		model.getSlider().setValue(0);
		model.setTranslationX(0);
		model.setTranslationY(0);	
		model.setStopTime(0);
		model.setRunAnimation(false);
			
		eraseB.setSelected(false);
		selectB.setSelected(false);
		drawB.setSelected(true);
		setCursor(drawCursor);
		
		repaint();
	}
	
	private void save(){
		String filePath;
		String dir = System.getProperty("user.dir");
		JFileChooser fc = new JFileChooser(dir);
		int rc = fc.showDialog(null, "Save As");
		if (rc == JFileChooser.APPROVE_OPTION)
		{
			File file = fc.getSelectedFile();
			filePath = file.getAbsolutePath();
			try
			{
				FileOutputStream outputFile = new FileOutputStream(file.getName()+".dat");
			    ObjectOutputStream output = new ObjectOutputStream(outputFile);
				output.writeObject(model.getDrawImage());
				
		        output.flush();
		        output.close();
		        outputFile.close();
		    }
			catch (Exception ex)
			{
		        System.out.println("Trouble writing to the file");
		    }
		}
	}
	
	private void open(){
		String dir = System.getProperty("user.dir");
		JFileChooser fc = new JFileChooser(dir);
		int rc = fc.showDialog(null, "Select Data File");
		if (rc == JFileChooser.APPROVE_OPTION)
		{
			File file = fc.getSelectedFile();
			try
			{
				FileInputStream inputFile = new FileInputStream(file.getName());
				System.out.println(inputFile);
		        ObjectInputStream input = new ObjectInputStream(inputFile);
		        ArrayList<ArrayList<Point>> inputObj=(ArrayList<ArrayList<Point>>)input.readObject();
		        startNew(); //To reset everything
		        model.setDrawImage(inputObj); 
		        input.close();
		        inputFile.close();
		        repaint();
		    }
			catch (Exception ex)
			{
		        System.out.println("Trouble reading the file");
		    }
		}
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		g.setColor(Color.white);
		
		g.fillRect(0, 0, getWidth(), getHeight());

		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
                  
    	if (model.getDrawImage().size()==0){ // for the first time when there is no object in the drawImage array
    		model.getDrawImage().add(model.getDrawPoints());
    		model.getStopTimeForImages().add(model.getStopTime());
    	}
    	Color curColor = Color.black;
    	
    	if (model.colorsUsed.size()==0)
    		model.colorsUsed.add(Color.black);
    	
    	if (!model.getRunAnimation() && (model.getDraw() || model.getErase())){
    		drawImage(g); //Draw previously drawn objects
	    	drawObject(g,model.getDrawPoints()); //Draw the points that are been dragged currently
    	}
    	
    	else if (model.getErase()){
    		setCursor(eraserCursor); 
    	}
    	
    	else if (model.getRunAnimation() || model.getSelect()){
        	//System.out.println(model.getSelectedImages().size()+" "+model.getDrawImage().size()+" "+model.getSelectedImages().size());
    		g2.setColor(Color.blue);    
        	Polygon selectedPolygon=new Polygon(); //Polygon of blue and red points displaying the portion of the screen that has been selected
        	
        	//System.out.println("Painting "+model.getRunAnimation());
        	
        	if (model.getSelectedImages().size()!=0){ //get rid of the selection sheet if the object is not contained properly
        		for (int i=0;i<model.getSelectionImagePoints().size();i++){
            		selectedPolygon.addPoint(model.getSelectionImagePoints().get(i).x+model.getTranslationX(), model.getSelectionImagePoints().get(i).y+model.getTranslationY());
            	}
	        	g2.setColor(Color.red);
            	g2.draw(selectedPolygon);	
            	Color c=new Color(0,0,0,65);
	            g2.setColor(c);
	        	g2.fill(selectedPolygon);
        	}
        	else{
        		drawImage(g);
        		drawObject(g,model.getSelectionImagePoints());
		        g2.setColor(Color.red);
		        for(int j = 0; j < model.getSelectionImagePoints().size(); j++) {
		            Point p = model.getSelectionImagePoints().get(j);
		            g2.fill(new Ellipse2D.Double(p.x-2, p.y-2, 4, 4));
		        }
        	}
       
      	    boolean hasBeenIncremented=false;
        	for (int i=0;i<model.getDrawImage().size();i++){
        		hasBeenIncremented=false;
        		curColor=Color.black;
        		if (i<model.colorsUsed.size()-1)
            		curColor=model.colorsUsed.get(i);
            	
        		g2.setColor(curColor);
            	for (int k=0;k<model.getSelectedImages().size();k++){
            		ArrayList<Point> drawP;
            		if (i<model.getDrawImage().size())
            			drawP = model.getDrawImage().get(i);
            		else
            			break;
            		ArrayList<Point> selectedDrawP = model.getSelectedImages().get(k);
            		if (selectedDrawP==drawP){
            			for (int l=0;l<selectedDrawP.size()-1;l++){
            				Point p,next;
            				int xDisplacement=0,yDisplacement=0;
            				
            				if (model.getRunAnimation()){
            					if (model.getCurTime()<model.getDisplacement().size()){
	            					xDisplacement=model.getDisplacement().get(model.getCurTime()).x;
	            					yDisplacement=model.getDisplacement().get(model.getCurTime()).y;
	            				}
            				}
            				else{
            					xDisplacement=model.getTranslationX();
            					yDisplacement=model.getTranslationY();
            				}
	            				
	            			p = new Point(selectedDrawP.get(l).x+xDisplacement,selectedDrawP.get(l).y+yDisplacement);
	    		    	    next = new Point(selectedDrawP.get(l+1).x+xDisplacement,selectedDrawP.get(l+1).y+yDisplacement);
            		       
            				if (p.distance(next)<=100)
 		    	            	g2.draw(new Line2D.Double(p, next));
            			}
            		}
            		else{
            			drawObject(g,drawP);
            		}
            		i++;
            		hasBeenIncremented=true;
            	}
            	if (hasBeenIncremented)i--;
        	}
        }
          
     }	
}
