# include <iostream>
#include <fstream>
using namespace std;

struct Node{
    string str;
    Node *next;
};
int main(int argc,char *argv[]){

    ifstream file;
    string str;
    Node *hashArray[1000]={NULL};
    int hashLengthArray[1000]={0};

    unsigned int val;
    file.open(argv[1]);

     while (file>>str){
        file>>str;
        val=0;
        for (int i=0;i<(int)str.length();i++){
            val=val+str[i];
        }
        if (hashArray[val%1000]==NULL){ //i.e hashArray is pointing to NULL
            Node *n= new Node;
            n->str=str;
            n->next=NULL;
            hashArray[val%1000]=n;
            hashLengthArray[val%1000]=hashLengthArray[val%1000]+1;
        }
        else{
            Node *n= new Node;
            n->str=str;
            Node *temp=hashArray[val%1000];
            int i=0;
            while (temp->next!=NULL){     //to reach to the end of the list
                temp=temp->next;
                i++;
            }
            temp->next=n;              //make next of hasArray point to the new Node
            hashLengthArray[val%1000]=hashLengthArray[val%1000]+1;
        }
    }
    for (int i=0;i<1000;i++){
        Node *temp=hashArray[i];
        while (temp!=NULL){     //to reach to the end of the list
            temp=temp->next;
        }
    }
    int max=0;
    for (int i=0;i<1000;i++){
        if (hashLengthArray[i]>max)
            max=i;
       // cout<<i<<" "<<hashLengthArray[i]<<endl;
    }
    cout<<max<<" "<<hashLengthArray[max]<<" ";
    Node *temp=hashArray[max];
        while (temp!=NULL){     //to reach to the end of the list
            cout<<temp->str<<" ";
            temp=temp->next;
        }
    return 1;
}


