
#include <iostream>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <time.h>

using namespace std;
struct Node{
    Node *next;
    int value;
};
int heuristic0(Node* &list,int n){
    int nodesVisited=0;
    Node *temp,*firstNode=new Node;
    firstNode=list;                 //Points to the first node in the list
    temp=list;

    while (temp->next!=NULL){
        if (temp->next->value==n){
            break;
        }
        temp=temp->next;
        nodesVisited++;
    }
     return nodesVisited;
}
int heuristic1(Node* &list,int n){
    int nodesVisited=0;
    Node *temp,*firstNode=new Node;
    firstNode=list;                 //Points to the first node in the list
    temp=list;
    while (temp->next!=NULL){
        if (temp->next->value==n){
            Node *temp2=new Node;

            temp2->value=firstNode->value;
            temp2->next=firstNode->next;

            firstNode->value=temp->next->value;
            firstNode->next=temp2;


            temp->next=temp->next->next;
            break;
        }
        temp=temp->next;
        nodesVisited++;
    }
    return nodesVisited;
}

int heuristic2(Node* &list,int n){
    int nodesVisited=0;
    Node *temp,*firstNode=new Node;
    firstNode=list;                 //Points to the first node in the list
    temp=list;

    while (temp->next!=NULL){
        if (temp->next->value==n){
            swap(temp->value,temp->next->value);
        }
        temp=temp->next;
        nodesVisited++;
    }
     return nodesVisited;
}

// There is an equal probability of getting each number from 1 to 100 when rand() is called.
int uniform(){

    //Choose a random number from 1-100
    int randomNumber=1+(rand()%100);        //1 is added coz rand() generates number from 0 to the end point of the range within which we want to generate random number
    return randomNumber;
}
/*  For Linear distribution, an array is created with 5050 elements where each number is stored the value of the number times,
    i.e there is one element with value 1, 2 elements with value 2 and so one, and then a random function will be called on that
    array where probability of fetching each 'element of array' would be 1/5050 but probability of finding each value would be
    number/5050
*/
int linear(){
    int array[5050];
    for (int i=0,k=0;i<100,k<5050;i++,k++){
        int j=0;
        while(j<=i)
        {
            array[k]=i+1;
            j++;
            if (j<=i)
                k++;
        }
    }
    int randomNumber=(rand()%5050);
    randomNumber=array[randomNumber];
    return randomNumber;
}
int flipCoin(int n){
    int randNumber=rand()%2;            //To randomly generate 0 or 1
    if (randNumber==1)
        return n;
    else
        return flipCoin(n-1);
}
int exp(){
    int randomNumber=1+(rand()%100);
    randomNumber=flipCoin(randomNumber);
    return randomNumber;
}

int main (int argc, char *argv[]) {

    int trainingRuns,trialRuns,randomNumber,nodesVisited;
    float avgNodesVisited;
    string s1,s2;
    Node *list=NULL;
    srand (time(NULL));

    //s1=argv[1];
    // s2=argv[2];
    // trainingRuns=atoi(argv[3]);
    // trialRuns=atoi(argv[4]);

    //Assign value to the list
    for (int i=100;i>=1;i--){
        Node *temp=new Node;
        temp->value=i;
        temp->next=list;
        list=temp;
     }

    s1="h1";
    s2="exp";
    trainingRuns=100;
    trialRuns=1000;

/*
    for (int k=0;k<trainingRuns;k++){
        if (s2=="uniform"){
        randomNumber=uniform();
        }
        else if (s2=="linear"){
            randomNumber=linear();
        }
        else if (s2=="exp"){
            randomNumber=exp();
        }
        if (s1=="none"){
            nodesVisited=heuristic0(list,randomNumber);
        }
        else if (s1=="h1"){
           nodesVisited=heuristic1(list,randomNumber);
        }
        else if (s1=="h2"){
            nodesVisited=heuristic2(list,randomNumber);
        }
    }*/
    nodesVisited=0;
    int sumOfNodesVisited=0;
    //for (int k=0;k<trialRuns;k++){
        if (s2=="uniform"){
        randomNumber=uniform();
        }
        else if (s2=="linear"){
            randomNumber=linear();
        }
        else if (s2=="exp"){
            randomNumber=exp();
        }
        if (s1=="none"){
            nodesVisited=heuristic0(list,randomNumber);
        }
        else if (s1=="h1"){
           nodesVisited=heuristic1(list,randomNumber);
        }
        else if (s1=="h2"){
            nodesVisited=heuristic2(list,randomNumber);
        }
        sumOfNodesVisited=sumOfNodesVisited+ nodesVisited;
    //}

  /*   Node *temp;//=new Node;
     temp=list;
     cout<<"random number: "<<randomNumber<<endl;
     while (temp->next!=NULL){
       cout<<temp->value;
        temp=temp->next;
     }
    */
    avgNodesVisited=sumOfNodesVisited/trialRuns;
    cout<<avgNodesVisited<<endl;

    return 0;
}
