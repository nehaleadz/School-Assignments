#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
using namespace std;
int nComparisons=0;
int partition(vector <int>&A, int l, int u, int p){

    int i=l+1,j;
    swap(A[l],A[p]);
    j=u;
    for (int t=l;t<u;t++)
    {
        while (i<u && A[i]<=A[l]){
            i=i+1;
            nComparisons++;
        }
        while (j>=l && A[j]>A[l]){
            j=j-1;
            nComparisons++;
        }
        if (j<i)
            break;
        else{
            swap (A[i],A[j]);
        }

    }
    swap (A[l],A[j]);
    return j-l+1;
}

int quickSelect(vector <int>&A,int l,int u,int m){
    int k,p;
    if (l<u){
        p=u;
        k=partition(A,l,u,p);
        if (m<k)
            quickSelect(A,l,k-1,m);
        else if (m>k)
            quickSelect(A,k+1,u,m-k);
        else
            return A[k];
    }
    else{
        return A[l];
    }
}
int main () {

    int pos;
    vector <int>v;
   /*
    v.push_back(6);
    v.push_back(5);
    v.push_back(3);
    v.push_back(1);
    v.push_back(8);
    v.push_back(7);
    v.push_back(2);
    v.push_back(4);
    */
    v.push_back(22);
    v.push_back(435);
    v.push_back(66);
    v.push_back(9);
    v.push_back(79);
    v.push_back(31);
    v.push_back(47);
    v.push_back(110);

    for (int i=1;i<8;i++){
        pos=quickSelect(v,1,7,i);
        cout<<"Position: "<<pos<<endl<<"Comparisons: "<<nComparisons<<endl<<endl;

    }
    return 0;
}
