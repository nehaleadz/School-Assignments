#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <math.h>
#include <fstream>
using namespace std;
int nComparisons=0;
int partition(vector <int>&A, int l, int u, int p){

    int i=l+1,j,nComparisons=0;
    swap(A[l],A[p]);
    j=u;
    for (int t=l;t<u;t++)
    {
        while (i<u && A[i]<=A[l]){
            i=i+1;
            nComparisons++;
        }
        while (j>=l && A[j]>A[l]){
            j=j-1;
            nComparisons++;
        }
        if (j<i)
            break;
        else{
            swap (A[i],A[j]);
        }

    }
    swap (A[l],A[j]);
    return j-l+1;
}
int classicQuickSelect(vector <int>&A,int l,int u,int m){
    int k,p;
    if (l<u){
        p=u;
        k=partition(A,l,u,p);
        if (m<k)
            classicQuickSelect(A,l,k-1,m);
        else if (m>k)
            classicQuickSelect(A,k+1,u,m-k);
        else
            return A[k];
    }
    else{
        return A[l];
    }
}

int alfredoQuickSelect(vector <int>&A,int l,int u,int m){

    int k,p,n=A.size(),s;
    if (l<u){

        s=sqrt(n);
        p=ceil((m/n)*s);
        k=partition(A,l,u,p);
        if (m<k)
            alfredoQuickSelect(A,l,k-1,m);
        else if (m>k)
            alfredoQuickSelect(A,k+1,u,m-k);
        else
        {
            return A[k];
        }
    }
    else{
        return A[l];
    }

}
int main (int argc, char *argv[]) {

   vector <int>v;
   ifstream file;
   int num,m;

   while (!file.eof()){
        file>>num;
        v.push_back(num);
        m=v[v.size()-1]; //the last element would be the value of m
        v.pop_back();
   }

    string s=argv[1];
    if (s=="-classic")
    {
        cout<<classicQuickSelect(v,0,v.size()-1,m)<<endl<<nComparisons;
    }
    else if (s=="-alfredo")
    {
        cout<<alfredoQuickSelect(v,0,v.size()-1,m)<<endl<<nComparisons;
    }

    return 0;
}
